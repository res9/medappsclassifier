from argparse import ArgumentParser
from pickle import dump
import pandas as pd
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from string import punctuation


def clean_text(*, text, words_to_remove):
    for symbol in punctuation:
        text = text.replace(symbol, ' ')
    words = text.lower().split()
    return word_tokenize(' '.join(_ for _ in words if _ not in words_to_remove))


def main(*, training_path, prediction_path, model_path, pipeline, encoding, language):
        
    xml_df = pd.read_csv(training_path, names=['Item_name', 'XML', 'Label'], encoding=encoding)
    predict_xmldf = pd.read_csv(prediction_path, names=['Item_name', 'XML'], encoding=encoding)
    
    predict_xmldf['XML'] = predict_xmldf.XML.apply(lambda _: clean_text(text=_, words_to_remove=stopwords.words(language)))
    xml_df['XML'] = predict_xmldf['XML']
    
    X = xml_df['Text']
    y = xml_df['Class']
    Z = predict_xmldf['Text']
    
    predict_files = predict_xmldf['Item_name']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2)

    pipeline.fit(X_train, y_train)
    dump(pipeline, open(model_path, 'wb'))

    print("Accuracy is: ", accuracy_score(pipeline.predict(X_test), y_test))

    for index, p in enumerate(pipeline.predict(Z)):
        print(predict_files[index], p)


if __name__ == '__main__':

    parser = ArgumentParser(description='XML training')
    parser.add_argument('training_path', type=str)
    parser.add_argument('prediction_path', type=str)
    parser.add_argument('model_path', type=str)
    parser.add_argument('--language', type=str, default='english')
    parser.add_argument('--encoding', type=str, default='unicode_escape')

    args = parser.parse_args()

    main(
        training_path=args.training_path,
        prediction_path=args.prediction_path,
        model_path=args.model_path,
        language=args.language,
        encoding=args.encoding,
        pipeline=Pipeline([
            ('bow', CountVectorizer()),  # strings to token integer counts, analyzer=text_processing
            ('tfidf', TfidfTransformer()),  # integer counts to weighted TF-IDF scores
            ('classifier', MultinomialNB()),  # train on TF-IDF vectors w/ Naive Bayes classifier
        ])
    )
