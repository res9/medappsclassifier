minute
Yes
Yes
 
DISA ELC exe Cert lord
MR. SOPA
Lr)
2. The following must be completed every
two years to maintain instructor status
The following steps must be completed
2. every two years to receive an NRP Provider
Course Completion Card
 

Instructional Resources

All resources are in one location and
keyword searchable. No need to
purchase a separate NRP Instructor
Manual or Instructor DVD
How To Become An
¥ NRP* Instructor

ot
Epinephrine Summary

Cee)
1:10,000 epinephrine (0.1 mg/mL)

Intravenous (preferred) or Intraosseous

Option: Endotracheal only while intravenous or intraosseous access is being
obtained

Preparation
Intravenous or Intraosseous = 1-mL syringe labeled “Epinephrine-IV”

Endotracheal = 3- to 5-mL syringe labeled “Epinephrine-ET only”

Intravenous or Intraosseous = 0.1 to 0.3 mL/kg
Endotracheal = 0.5 to 1 mL/kg

eee Lec
Rapidly—as quickly as possible
Intravenous or Intraosseous: Flush with 0.5 to 1 mL normal saline
Endotracheal: PPV breaths to distribute into lungs

Repeat every 3 to 5 minutes if heart rate remains less than 60 bpm.
Table 3-1. Target Pre-ductal Spo,

 

 

 

 

 

 

After Birth
1 min 60%-65%
2 min 65%-70%
3 min 70%-75%
4 min 75%-80%
5 min 80%-85%
10 min 85%-95%

 

 

 

 
Volume Expander Summary

Normal saline (0.9% NaCl)

Suspected anemia: O-negative packed red blood cells

Intravenous or Intraosseous

30- to 60-mL syringe (labeled)

Oretachi)

Over 5 to 10 minutes

(Use caution with preterm newborns less than 30 weeks’ gestation.)
Gohwreonatohmsen
Team briefing and equipment check.

Stay with mother for routine care:
Warm and maintain normal
temperature, position airway,
clear secretions if needed, dry,
ongoing evaluation.

 

  

 

Warm and maintain normal
temperature, position airway, clear @
secretions if needed, dry, stimulate.

lo

1
minute

 

 

 

  
 
   
     
  

 

 

Ye
es Yes
PPV. Position and clear airway.
Spo, monitor. Spo, monitor. ®
Consider ECG monitor. Supplemental Oz as needed.

 

Consider CPAP.

 

Post-resuscitation care.
Team debriefing.

 

 

 

Yes
Check chest movement.
Ventilation corrective steps if Pre-ductal Spos Target
needed. po2 g
ETT or laryngeal mask if needed. 4 min 60%—65%
O 2 min 65%—70%
3 min 70%-75%
Ne 4min 75%-80%

5 min 80%—85%
Yes 10 min 85%—-95%

 

Intubate if not already done.
Chest compressions.
Coordinate with PPV.

100% Oo.
ECG monitor.

 

 

Yes

 

IV epinephrine.

If HR persistently below 60 bpm: @
consider hypovolemia,
consider pneumothorax.

 

 

 

Weiner, G. M., & Zaichkin, J. (2016). Textbook of Neonatal Resuscitation, 7th Edition. Elk Grove Village, IL:
American Academy of Pediatrics.
Team briefing and equipment check.

Stay with mother for routine care:
Warm and maintain normal
temperature, position airway,
clear secretions if needed, dry,
ongoing evaluation.

 

  

 

Warm and maintain normal
4 temperature, position airway, clear
secretions if needed, dry, stimulate.

J

 

 

minute

 

  
 
   
     
  

 

Yes Yes

 

PPV.
Spo, monitor.

Position and clear airway.
Spo, monitor.

Consider ECG monitor. Supplemental Oz as needed.

 

 

 

 

 

Consider CPAP.
No Post-resuscitation care.
Team debriefing.
Yes
Check chest movement.
Ventilation corrective steps if _
ae Pre-ductal Spoz2 Target
ETT or laryngeal mask if needed. 4 min 60%-65%
2 min 65%—70%
N 3 min 70%-75%
° 4min 75%-80%
5 min 80%—85%
Yes 10 min 85%—-95%

 

Intubate if not already done.
Chest compressions.
Coordinate with PPV.

100% Oo.
ECG monitor.

 

 

Yes

 

IV epinephrine.

If HR persistently below 60 bpm:
consider hypovolemia,
consider pneumothorax.

 

 

 

Weiner, G. M., & Zaichkin, J. (2016). Textbook of Neonatal Resuscitation, 7th Edition. Elk Grove Village, IL:
American Academy of Pediatrics.
 

 

Neonatal

Birth

 
  
  

Yes—stay

 
  

Term gestation?
Breathing or crying?
Good tone?

No
Warm, clear airway if necessary,
dry, stimulate

HR below 100 bpm,
gasping, or apnea?

  

HR below 100 bpm?

Take vent ion
corrective steps

 
 

HR below 60 bpm?

with mother

or persistent

 
 
 
  
 
   
    
 
  
  

Routine Care

+ Provide warmth

* Clear airway if necessary
*Dry

* Ongoing evaluation

    
  
 
   
 
    
  
   
 
   

Labored
breathing

cyanosis?

Clear airway
Spo, monitoring
Consider CPAP

Post-resuscitation

care

Targeted Pre-ductal Sro,
Consider intubation After Birth
Chest compressions .
Coordinate with PPV min 60%-65%
2min 65%-70%
Take ventilati 3 min 70%-75%
fake ventilation .
corrective steps TR below 60 bpm? 4min 75%-80%
Intubateit ny 5 min 80%-85%
t rise!
oe 10min ——_85%-95%

 

Consider:
+ Hypovolemia
+ Pneumothorax

 

 

 
Neonatal

 
 
 
  

 
  
  

 
  

    
    
 
  
  

   
     
   
    
   
   

  

Birth
! Yes—stay Routine Care
| Term gestation? with mother + Provide warmth
I Breathing or crying? * Clear airway if necessary
! Good tone? *Dry ;
I * Ongoing evaluation
1
I No
' Warm, clear airway if necessary,
I dry, stimulate
1
1
\ Labored
HR below 100 bpm, breathing
30sec gasping, or apnea? or persistent
' cyanosis?
1
1
1
1 Clear airway
1 Spo, monitoring
| Consider CPAP
60sec

HR below 100 bpm?

    

Take ven' ion
corrective steps Post-resuscitation
care

HR below 60 bpm?

Targeted Pre-ductal Sro,
Consider intubation After Birth

Chest compressions .

Coordinate with PPV min 60%-65%

2min 65%-70%

Take ventilat 3 min 70%-75%
fake ventilation .

corrective steps TAR below 60 bpm? 4min 75%-80%

Intubate if cana 5 min 80%-85%

hest rise!
“oe 10 min 85%-95%

Consider:
+ Hypovolemia
+ Pneumothorax

 
 

 

Neonatal
Resuscitation
. Program™

é American

Heart
Associatione
SME RELL
 

oe

Ventilations

iNRP Metronome
http://www?2.aap.ory

(© 2016 American Academy of Pediatrics
=
 
a

 
 
‘KAP
 

 

Neonatal
Resuscitation
. Program™

Aa

‘American
Heart
Association.
Neonatal
Resuscitation
| Program™

Se ox
Neonatal
Resuscitation
| Program™

‘American
Heart
Association.

 
 

 

 

by

 

 

Neonatal
Resuscitation
. Program™

American
Heart
Associatione
Neonatal
Resuscitation
. Program™

EZ American
Heart
Associations

 
Neonatal
Resuscitation
. Program™

Zé American
Heart
Associations

 
 

 

 

by

 

 

Neonatal
Resuscitation
. Program™

a American
Heart
Associations
Neonatal
Resuscitation
Program’

& ox

 
Neonatal
Resuscitation
Program™

american
Heart
Association.

 
 

 

 

oy

 

 

Neonatal
Resuscitation
. Program™

EZ American

Heart
Associatione
Neonatal
Resuscitation
. Program™

é American
Heart
Associatione

 
Neonatal
Resuscitation
. Program™

a American

Heart
Associations

 
-
ial
>
Sh

 

 
Py
CTL
Py
CTL
Py
ae
7)
ae
7)
ae
7)
ae
7)
ae
Py
ae
minute
Yes
Yes
 
DISA ELC exe Cert lord
MR. SOPA
Lr)
2. The following must be completed every
two years to maintain instructor status
The following steps must be completed
2. every two years to receive an NRP Provider
Course Completion Card
 

Instructional Resources

All resources are in one location and
keyword searchable. No need to
purchase a separate NRP Instructor
Manual or Instructor DVD
How To Become An
¥ NRP* Instructor

ot
Epinephrine Summary

Cee)
1:10,000 epinephrine (0.1 mg/mL)

Intravenous (preferred) or Intraosseous

Option: Endotracheal only while intravenous or intraosseous access is being
obtained

Preparation
Intravenous or Intraosseous = 1-mL syringe labeled “Epinephrine-IV”

Endotracheal = 3- to 5-mL syringe labeled “Epinephrine-ET only”

Intravenous or Intraosseous = 0.1 to 0.3 mL/kg
Endotracheal = 0.5 to 1 mL/kg

eee Lec
Rapidly—as quickly as possible
Intravenous or Intraosseous: Flush with 0.5 to 1 mL normal saline
Endotracheal: PPV breaths to distribute into lungs

Repeat every 3 to 5 minutes if heart rate remains less than 60 bpm.
Table 3-1. Target Pre-ductal Spo,

 

 

 

 

 

 

After Birth
1 min 60%-65%
2 min 65%-70%
3 min 70%-75%
4 min 75%-80%
5 min 80%-85%
10 min 85%-95%

 

 

 

 
Volume Expander Summary

Normal saline (0.9% NaCl)

Suspected anemia: O-negative packed red blood cells

Intravenous or Intraosseous

30- to 60-mL syringe (labeled)

Oretachi)

Over 5 to 10 minutes

(Use caution with preterm newborns less than 30 weeks’ gestation.)
Gohwreonatohmsen
Team briefing and equipment check.

Stay with mother for routine care:
Warm and maintain normal
temperature, position airway,
clear secretions if needed, dry,
ongoing evaluation.

 

  

 

Warm and maintain normal
temperature, position airway, clear @
secretions if needed, dry, stimulate.

lo

1
minute

 

 

 

  
 
   
     
  

 

 

Ye
es Yes
PPV. Position and clear airway.
Spo, monitor. Spo, monitor. ®
Consider ECG monitor. Supplemental Oz as needed.

 

Consider CPAP.

 

Post-resuscitation care.
Team debriefing.

 

 

 

Yes
Check chest movement.
Ventilation corrective steps if Pre-ductal Spos Target
needed. po2 g
ETT or laryngeal mask if needed. 4 min 60%—65%
O 2 min 65%—70%
3 min 70%-75%
Ne 4min 75%-80%

5 min 80%—85%
Yes 10 min 85%—-95%

 

Intubate if not already done.
Chest compressions.
Coordinate with PPV.

100% Oo.
ECG monitor.

 

 

Yes

 

IV epinephrine.

If HR persistently below 60 bpm: @
consider hypovolemia,
consider pneumothorax.

 

 

 

Weiner, G. M., & Zaichkin, J. (2016). Textbook of Neonatal Resuscitation, 7th Edition. Elk Grove Village, IL:
American Academy of Pediatrics.
Team briefing and equipment check.

Stay with mother for routine care:
Warm and maintain normal
temperature, position airway,
clear secretions if needed, dry,
ongoing evaluation.

 

  

 

Warm and maintain normal
4 temperature, position airway, clear
secretions if needed, dry, stimulate.

J

 

 

minute

 

  
 
   
     
  

 

Yes Yes

 

PPV.
Spo, monitor.

Position and clear airway.
Spo, monitor.

Consider ECG monitor. Supplemental Oz as needed.

 

 

 

 

 

Consider CPAP.
No Post-resuscitation care.
Team debriefing.
Yes
Check chest movement.
Ventilation corrective steps if _
ae Pre-ductal Spoz2 Target
ETT or laryngeal mask if needed. 4 min 60%-65%
2 min 65%—70%
N 3 min 70%-75%
° 4min 75%-80%
5 min 80%—85%
Yes 10 min 85%—-95%

 

Intubate if not already done.
Chest compressions.
Coordinate with PPV.

100% Oo.
ECG monitor.

 

 

Yes

 

IV epinephrine.

If HR persistently below 60 bpm:
consider hypovolemia,
consider pneumothorax.

 

 

 

Weiner, G. M., & Zaichkin, J. (2016). Textbook of Neonatal Resuscitation, 7th Edition. Elk Grove Village, IL:
American Academy of Pediatrics.
 

 

Neonatal

Birth

 
  
  

Yes—stay

 
  

Term gestation?
Breathing or crying?
Good tone?

No
Warm, clear airway if necessary,
dry, stimulate

HR below 100 bpm,
gasping, or apnea?

  

HR below 100 bpm?

Take vent ion
corrective steps

 
 

HR below 60 bpm?

with mother

or persistent

 
 
 
  
 
   
    
 
  
  

Routine Care

+ Provide warmth

* Clear airway if necessary
*Dry

* Ongoing evaluation

    
  
 
   
 
    
  
   
 
   

Labored
breathing

cyanosis?

Clear airway
Spo, monitoring
Consider CPAP

Post-resuscitation

care

Targeted Pre-ductal Sro,
Consider intubation After Birth
Chest compressions .
Coordinate with PPV min 60%-65%
2min 65%-70%
Take ventilati 3 min 70%-75%
fake ventilation .
corrective steps TR below 60 bpm? 4min 75%-80%
Intubateit ny 5 min 80%-85%
t rise!
oe 10min ——_85%-95%

 

Consider:
+ Hypovolemia
+ Pneumothorax

 

 

 
Neonatal

 
 
 
  

 
  
  

 
  

    
    
 
  
  

   
     
   
    
   
   

  

Birth
! Yes—stay Routine Care
| Term gestation? with mother + Provide warmth
I Breathing or crying? * Clear airway if necessary
! Good tone? *Dry ;
I * Ongoing evaluation
1
I No
' Warm, clear airway if necessary,
I dry, stimulate
1
1
\ Labored
HR below 100 bpm, breathing
30sec gasping, or apnea? or persistent
' cyanosis?
1
1
1
1 Clear airway
1 Spo, monitoring
| Consider CPAP
60sec

HR below 100 bpm?

    

Take ven' ion
corrective steps Post-resuscitation
care

HR below 60 bpm?

Targeted Pre-ductal Sro,
Consider intubation After Birth

Chest compressions .

Coordinate with PPV min 60%-65%

2min 65%-70%

Take ventilat 3 min 70%-75%
fake ventilation .

corrective steps TAR below 60 bpm? 4min 75%-80%

Intubate if cana 5 min 80%-85%

hest rise!
“oe 10 min 85%-95%

Consider:
+ Hypovolemia
+ Pneumothorax

 
 

 

Neonatal
Resuscitation
. Program™

é American

Heart
Associatione
SME RELL
 

oe

Ventilations

iNRP Metronome
http://www?2.aap.ory

(© 2016 American Academy of Pediatrics
=
 
-
ial
>
Sh

 

 
Neonatal
Resuscitation
. Program™

a American

Heart
Associations

 
Py
CTL
Py
CTL
Py
ae
7)
ae
7)
ae
7)
ae
7)
ae
Py
ae
