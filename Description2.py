import glob
import os
import re
from glob import glob
import pandas as pd
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
import pickle



# XML
csv_file = 'Groundtruthxml_new2.csv'
predict_csv_file='Groundtruthxml_new2.csv'

xml_df = pd.read_csv(csv_file,names=['Item_Name','XML','Label'],sep=',', encoding='utf-8')
predict_xml_df=pd.read_csv(predict_csv_file,names=['Item_Name','XML'],sep=',', encoding='utf-8')



stop_words = stopwords.words('english')
def CleanText(text):
    #Lower the text
    try:
        text = text.lower()
    except:
        print('ignoring text')

    #Remove all punctuation
    try:
        text = re.sub(r'[^\w\s]','',text)
    except:
        print('ignoring text')

    text = word_tokenize(str(text))

    return text



xml_df['XML'] = xml_df.XML.apply(CleanText)
xml_df['XML'] = xml_df.XML.apply(lambda text: " ".join(w for w in text if w not in stop_words))
xml_df.head()

predict_xml_df['XML'] = predict_xml_df.XML.apply(CleanText)
predict_xml_df['XML'] = predict_xml_df.XML.apply(lambda text: " ".join(w for w in text if w not in stop_words))

#Building Model
#Model Selection and Validation

X = xml_df['XML']
y = xml_df['Label']
Z = predict_xml_df['XML']
predict_namexml=predict_xml_df['Item_Name']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2)

pipeline = Pipeline([
    ('bow', CountVectorizer()),  # strings to token integer counts, analyzer=text_processing
    ('tfidf', TfidfTransformer()),  # integer counts to weighted TF-IDF scores
    ('classifier', MultinomialNB()),  # train on TF-IDF vectors w/ Naive Bayes classifier
])
# Fit the model on training set
pipeline.fit(X_train, y_train)
# save the model to disk
filename = 'finalized_model.pkl'
pickle.dump(pipeline, open(filename, 'wb'))

# Generate prediction on our test data
predictions = pipeline.predict(X_test)
print("Accuracy is: ", accuracy_score(predictions, y_test))

final_prediction_xml = pipeline.predict(Z.values.astype('U'))

i = 0
for p in final_prediction_xml:
    print(predict_namexml[i], p)
    i += 1




