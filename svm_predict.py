import sys


import csv
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
csv.field_size_limit(sys.maxsize)

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

import re, string, unicodedata
import nltk
import contractions
import inflect
from bs4 import BeautifulSoup
from nltk import word_tokenize, sent_tokenize
from nltk.corpus import stopwords
from nltk.stem import LancasterStemmer, WordNetLemmatizer

import nltk
from nltk.corpus import stopwords
stopwords = stopwords.words('english')

from nltk.corpus import words as meaningful_words

from nltk.stem import WordNetLemmatizer
wordnet_lemmatizer = WordNetLemmatizer()


ff = open('filterSVMwords.txt','r')

filter_words = []

for line in ff:
	line = line.strip('\n')
	filter_words.append(line)

import re
def preprocess(doc):
    description = doc
    description = re.sub(r'http:\/\/.*[\r\n]*','',str(description))#, flags=re.MULTILINE|re.UNICODE)
    description = re.sub(r'https?:\/\/.*[\r\n]*','',description)#, flags=re.MULTILINE|re.UNICODE)
    description = re.sub(r'[^\w\s]',' ',description)#, flags=re.UNICODE)
    #description = re.sub(r'[^\x00-\x7f]',r'', description)
    #description = re.sub(r'\s\s+', ' ', description)
    description = re.sub(r'\t+', ' ', description)
    description = re.sub(r'\r+', ' ', description)
    #description = re.sub(r'[0-9]+', ' ', description)
    #description = re.sub(r'_+', ' ', description)
    description = re.sub(r'\s\s+', ' ', description)
    return description

def remove_non_ascii(words):
    """Remove non-ASCII characters from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = unicodedata.normalize('NFKD', word).encode('ascii', 'ignore').decode('utf-8', 'ignore')
        new_words.append(new_word)
    return new_words

def to_lowercase(words):
    """Convert all characters to lowercase from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = word.lower()
        new_words.append(new_word)
    return new_words

def remove_punctuation(words):
    """Remove punctuation from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = re.sub(r'[^\w\s]', '', word)
        if new_word != '':
            new_words.append(new_word)
    return new_words

def replace_numbers(words):
    """Replace all interger occurrences in list of tokenized words with textual representation"""
    p = inflect.engine()
    new_words = []
    for word in words:
        if word.isdigit():
            new_word = p.number_to_words(word)
            new_words.append(new_word)
        else:
            new_words.append(word)
    return new_words

def remove_stopwords(words):
    """Remove stop words from list of tokenized words"""
    new_words = []
    for word in words:
        if word not in stopwords:
            new_words.append(word)
    return new_words

def stem_words(words):
    """Stem words in list of tokenized words"""
    stemmer = LancasterStemmer()
    stems = []
    for word in words:
        stem = stemmer.stem(word)
        stems.append(stem)
    return stems

def lemmatize_verbs(words):
    """Lemmatize verbs in list of tokenized words"""
    lemmatizer = WordNetLemmatizer()
    lemmas = []
    for word in words:
        lemma = lemmatizer.lemmatize(word, pos='v')
        lemmas.append(lemma)
    return lemmas

def normalize(words):
    words = remove_non_ascii(words)
    words = to_lowercase(words)
    words = remove_punctuation(words)
    words = replace_numbers(words)
    words = remove_stopwords(words)
    return words

#words = normalize(words)

print("Loading Corpus.....")

vocabulary = []

corpus = []
labels = []
apps=[]
corpus_predict=[]
apps_predict=[]

#######################Ground Truth Corpus############################################
with open('Patient_new.csv',encoding="utf8", errors='ignore') as f:
    csvreader = csv.reader(f,delimiter=',')
    for row in csvreader:
        #corpus.append(row[-1].lower().encode('utf-8'))
        row[1]=preprocess(row[1])
        #print(row[1])
        words = row[1].lower().split(' ')
        document_words = []
        words = normalize(words)

        for w in words:
            #filtering all one letter characters
            if (len(w) <= 2):
                continue
            # filtering Xae... Xcb...words like this
            if (w[0]=='x' or w in filter_words):
                continue
            #filtering all words that have any letter other than a-z
            if w.isalpha():
                document_words.append(w)

        document = ' '.join(document_words)
        corpus.append(document)
        #labels.append(int(row[0]))
        if(len(corpus)>=33):
            apps_predict.append(str(row[0]))
            continue

        if row[2] == 'device':
            labels.append(int(1))
        elif row[2] == 'non device':
            labels.append(int(0))
        else:
            labels.append(int(0))
        apps.append(str(row[0]))
f.close()

print ("Corpus size:"+str(len(corpus)))



# ******************** TF Array ***********************************************************

vectorizer = CountVectorizer(min_df=1, encoding='utf-8', stop_words=stopwords)

X = vectorizer.fit_transform(corpus)

#doc_term_matrix = X.toarray()
doc_term_matrix = X

#l=list(vectorizer.vocabulary_.keys())
#print(len(l))







# ******************** TF-IDF Score ********************************************

transformer = TfidfTransformer(smooth_idf=True)

document_term_tfidf = transformer.fit_transform(doc_term_matrix)

#document_term_tfidf = document_term_tfidf.toarray()

#print(document_term_tfidf)




#  ****************** TF-IDF ranking per dcoument **********************************************

def top_tfidf_feats(row, features, top_n=25):
    ''' Get top n tfidf values in row and return them with their corresponding feature names.'''
    topn_ids = np.argsort(row)[::-1][:top_n]
    top_feats = [(features[i], row[i]) for i in topn_ids]
    df = pd.DataFrame(top_feats)
    df.columns = ['feature', 'tfidf']
    return df

def top_feats_in_doc(Xtr, features, row_id, top_n=25):
    ''' Top tfidf features in specific document (matrix row) '''
    row = np.squeeze(Xtr[row_id].toarray())
    return top_tfidf_feats(row, features, top_n)

def top_mean_feats(Xtr, features, grp_ids=None, min_tfidf=0.1, top_n=25):
    ''' Return the top n features that on average are most important amongst documents in rows
        indentified by indices in grp_ids. '''
    if grp_ids:
        D = Xtr[grp_ids].toarray()
    else:
        D = Xtr.toarray()

    D[D < min_tfidf] = 0
    tfidf_means = np.mean(D, axis=0)
    return top_tfidf_feats(tfidf_means, features, top_n)

def top_feats_by_class(Xtr, y, features, min_tfidf=0.1, top_n=25):
    ''' Return a list of dfs, where each df holds top_n features and their mean tfidf value
        calculated across documents with the same class label. '''
    dfs = []
    labels = np.unique(y)
    for label in labels:
        ids = np.where(y==label)
        feats_df = top_mean_feats(Xtr, features, ids, min_tfidf=min_tfidf, top_n=top_n)
        feats_df.label = label
        dfs.append(feats_df)
    return dfs

def plot_tfidf_classfeats_h(dfs):
    ''' Plot the data frames returned by the function plot_tfidf_classfeats(). '''
    fig = plt.figure(figsize=(12, 9), facecolor="w")
    x = np.arange(len(dfs[0]))
    for i, df in enumerate(dfs):
        ax = fig.add_subplot(1, len(dfs), i+1)
        ax.spines["top"].set_visible(False)
        ax.spines["right"].set_visible(False)
        ax.set_frame_on(False)
        ax.get_xaxis().tick_bottom()
        ax.get_yaxis().tick_left()
        ax.set_xlabel("Mean Tf-Idf Score", labelpad=16, fontsize=14)
        ax.set_title("label = " + str(df.label), fontsize=16)
        ax.ticklabel_format(axis='x', style='sci', scilimits=(-2,2))
        ax.barh(x, df.tfidf, align='center', color='#3F5D7D')
        ax.set_yticks(x)
        ax.set_ylim([-1, x[-1]+1])
        yticks = ax.set_yticklabels(df.feature)
        plt.subplots_adjust(bottom=0.09, right=0.97, left=0.15, top=0.95, wspace=0.52)
    plt.show()

## get the feature names

feature_names = vectorizer.get_feature_names()

# get the document that we want to extract keywords from
app_index=201


# res=top_feats_in_doc(document_term_tfidf,feature_names,app_index,10)
# print("....top keywords in documnet X...")
# print(apps[app_index])
# print(res)


# top_words=top_mean_feats(document_term_tfidf,feature_names,None,0.1,25)
# print("top keywords in whole documnet....")
# print(top_words)


#
#
# dfs=top_feats_by_class(document_term_tfidf,labels,feature_names,0.1,20)
# plot_tfidf_classfeats_h(dfs)



# ******************** apply SVM ***********************************************

import numpy as np
from sklearn import linear_model

model = linear_model.SGDClassifier()
#from sklearn.naive_bayes import MultinomialNB
#model=MultinomialNB()

mini_batch_size = 10
num_mini_batches = 3

for i in range(num_mini_batches):
    X = document_term_tfidf[i*mini_batch_size : i*mini_batch_size + mini_batch_size]
    y = labels[i*mini_batch_size : i*mini_batch_size + mini_batch_size]
    model.partial_fit(X,y,classes=[0,1])

 ## 680 was ground truth app size
X_test = document_term_tfidf[30 : 40]
y_true = labels[30 : 40]
y_pred = model.predict(X_test)

print("Accuracy result......")
tp = 0
test_data_size = X_test.shape[0]
for i in range(test_data_size): #(len(X_test)):
    #print (type( y_true[i]))
    #print (type(int(y_pred[i])))
    if y_true[i] == int(y_pred[i]):
        tp += 1
    #print (y_true[i],y_pred[i])
    #print(y_pred[i])

print(test_data_size)
print(tp)
print ("accuracy:%d"%((tp/ test_data_size)*100))


fw = open('prediction_result_description_reference_vs_non-reference_classifier_new.csv','w+')
fw.write('AppName'+','+'Reference')
fw.write('\n')
print("prediction result......")
Z=document_term_tfidf[41 : len(corpus)]
Z_predict=model.predict(Z)
predcit_data_size=Z.shape[0]
for i in range(predcit_data_size):
    #print(apps_predict[i],str(Z_predict[i]))
    fw.write(apps_predict[i]+','+str(Z_predict[i]))
    fw.write('\n')
