import glob
import os
import re
from glob import glob
import csv

import pandas as pd
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer





def CleanText(text):
    text = text[0]
        # Lower the text
    text = text.lower()
        # Remove all punctuation
    text = re.sub(r'[^\w\s]', '', text)
        # Tokenize the text
    text = word_tokenize(text)
    return text

stop_words = stopwords.words('english')

# XML

trainingxml = 'training_xml.csv'
xml_df = pd.read_csv(trainingxml, names=['Item_name', 'XML', 'Label'], encoding='unicode_escape')

predictionxml = 'predicting_xml.csv'
predict_xmldf = pd.read_csv(predictionxml, names=['Item_name', 'XML'], encoding='unicode_escape')

import json
from pprint import pprint

# Description

with open('output.labeled.json') as f:
    training_desc = json.load(f)

from collections import namedtuple

Item = namedtuple('Item', ['name', 'description', 'label'])

def sanitize_line(line):
   if any(word.upper() in line.upper() for word in stop_words):
      return ''
   else:
      return CleanText(line)

def extract_description(lines):
   return ''.join([sanitize_line(_) for _ in lines])

def extract_item(element):
   return Item(
      description=extract_description(element['Description']),
    name=element['Item_name'],
      label=element['label']
   )

items = tuple(extract_item(_) for _ in training_desc)

descrip_df= items




pprint(descrip_df)

with open('output.unlabeled.json') as g:
    predict_desc = json.load(g)


Itema = namedtuple('Itema', ['name', 'description'])

def extract_item1(element):
   return Itema(
      description=extract_description(element['Description']),
    name=element['Item_name'],
   )

items1 = tuple(extract_item1(_) for _ in predict_desc)
predict_descripdf = items1
pprint(predict_descripdf)


# Tesseractfunction
def createDataset(root_dir):
    dataset_list = []
    data_to_predict_list = []
    class_dirs = glob(root_dir + '/*')
    # Go through each sub_directory, which represents a different ground truth class
    for class_dir in class_dirs:
        # print(class_dir)
        if (os.path.isfile(class_dir)):
            with open(class_dir, 'r', encoding='utf-8') as f:
                file_text = f.read().replace('\n', '')
            data_to_predict_list.append([class_dir, file_text])
        for root, dirs, files in os.walk(class_dir):
            # For each file, read the text and append it to the dataset
            for file in files:
                # print(class_dir)
                # print(file)
                with open(os.path.join(class_dir, file), 'r', encoding='utf-8') as f:
                    file_text = f.read().replace('\n', '')
                dataset_list.append([file, file_text, class_dir.split(' ')[1]])
    # Create the pandas dataset
    tess_df = pd.DataFrame(dataset_list, columns=['Item_name', 'Text', 'Class'])
    predict_df = pd.DataFrame(data_to_predict_list, columns=['Item_name', 'Text'])
    return tess_df, predict_df


root_dir = 'TesseractOutput'
tess_df, predict_df = createDataset(root_dir)

tess_df.head()
predict_df.head()

tess_df['Text'] = tess_df.Text.apply(CleanText)
tess_df['Text'] = tess_df.Text.apply(lambda text: " ".join(w for w in text if w not in stop_words))

#descrip_df = CleanText(descrip_df)
#descrip_df['Item_name'] = descrip_df.Item_name.apply(lambda text: " ".join(w for w in text if w not in stop_words))

#predict_df['Text'] = predict_df.Text.apply(CleanText)
#predict_df['Text'] = predict_df.Text.apply(lambda text: " ".join(w for w in text if w not in stop_words))

predict_xmldf['XML'] = predict_xmldf.XML.apply(CleanText)
predict_xmldf['XML'] = predict_xmldf.XML.apply(lambda text: " ".join(w for w in text if w not in stop_words))

#predict_descripdf['Description'] = predict_descripdf.Description.apply(CleanText)
#predict_descripdf['Description'] = predict_descripdf.Description.apply(lambda text: " ".join(w for w in text if w not in stop_words))

tess_df.head()
predict_df.head()

# print(tess_df)
# print(predict_df)

# Building Model
# Model Selection and Validation
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
import pickle

X = tess_df['Text']
y = tess_df['Class']
Z = predict_df['Text']

predict_files = predict_df['Item_name']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2)

pipeline = Pipeline([
    ('bow', CountVectorizer()),  # strings to token integer counts, analyzer=text_processing
    ('tfidf', TfidfTransformer()),  # integer counts to weighted TF-IDF scores
    ('classifier', MultinomialNB()),  # train on TF-IDF vectors w/ Naive Bayes classifier
])
# Fit the model on training set
pipeline.fit(X_train, y_train)
# save the model to disk
filename = 'finalized_model.pkl'
pickle.dump(pipeline, open(filename, 'wb'))

# Generate prediction on our test data
predictions = pipeline.predict(X_test)
print("Accuracy is: ", accuracy_score(predictions, y_test))

final_prediction = pipeline.predict(Z)

i = 0
for p in final_prediction:
    print(predict_files[i], p)
    i += 1

# XML Prediction

X = xml_df['XML']
y = xml_df['Label']
Z = predict_xmldf['XML']

predict_namexml = predict_xmldf['Item_name']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2)

pipeline = Pipeline([
    ('bow', CountVectorizer()),  # strings to token integer counts, analyzer=text_processing
    ('tfidf', TfidfTransformer()),  # integer counts to weighted TF-IDF scores
    ('classifier', MultinomialNB()),  # train on TF-IDF vectors w/ Naive Bayes classifier
])
# Fit the model on training set
pipeline.fit(X_train, y_train)
# save the model to disk
filename = 'finalized_model.pkl'
pickle.dump(pipeline, open(filename, 'wb'))

# Generate prediction on our test data
predictions = pipeline.predict(X_test)
print("Accuracy is: ", accuracy_score(predictions, y_test))

final_predictionxml = pipeline.predict(Z)

i = 0
for p in final_predictionxml:
    print(predict_namexml[i], p)
    i += 1

# Description Prediction

X = descrip_df['Description']
y = descrip_df['label']
Z = predict_descripdf['Description']

predict_descrip_name = predict_descripdf['Item_name']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2)

pipeline = Pipeline([
    ('bow', CountVectorizer()),  # strings to token integer counts, analyzer=text_processing
    ('tfidf', TfidfTransformer()),  # integer counts to weighted TF-IDF scores
    ('classifier', MultinomialNB()),  # train on TF-IDF vectors w/ Naive Bayes classifier
])
# Fit the model on training set
pipeline.fit(X_train, y_train)
# save the model to disk
filename = 'finalized_model.pkl'
pickle.dump(pipeline, open(filename, 'wb'))

# Generate prediction on our test data
predictions = pipeline.predict(X_test)
print("Accuracy is: ", accuracy_score(predictions, y_test))

final_prediction_descript = pipeline.predict(Z)

i = 0
for p in final_prediction:
    print(predict_files[i], p)
    i += 1
