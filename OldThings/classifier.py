import glob
import os
import re
from glob import glob

import pandas as pd
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer

input_file = 'med.json'
descrip_df = pd.read_json(input_file)[['Description','Item_name']]
descrip_df.head()


csv_file = 'med_free_apps_xmlwords.csv'
xml_df = pd.read_csv(csv_file,names=['Item_name','XML'])


# Tesseractfunction
def createDataset(root_dir):
    dataset_list = []
    data_to_predict_list = []
    class_dirs = glob(root_dir + '/*')
    # Go through each sub_directory, which represents a different ground truth class
    for class_dir in class_dirs:
        # print(class_dir)
        if (os.path.isfile(class_dir)):
            with open(class_dir, 'r', encoding='utf-8') as f:
                file_text = f.read().replace('\n', '')
            data_to_predict_list.append([class_dir, file_text])
        for root, dirs, files in os.walk(class_dir):
            # For each file, read the text and append it to the dataset
            for file in files:
                # print(class_dir)
                # print(file)
                with open(os.path.join(class_dir, file), 'r', encoding='utf-8') as f:
                    file_text = f.read().replace('\n', '')
                dataset_list.append([file, file_text, class_dir.split(' ')[1]])
                # Create the pandas dataset
    tess_df = pd.DataFrame(dataset_list, columns=['Item_name', 'Text', 'Class'])
    predict_df = pd.DataFrame(data_to_predict_list, columns=['Item_name', 'Text'])
    return tess_df, predict_df



root_dir = 'TesseractOutput'
tess_df = createDataset(root_dir)

print(tess_df)

stop_words = stopwords.words('english')
def CleanText(text):
    #Lower the text
    text = text.lower()

    #Remove all punctuation
    text = re.sub(r'[^\w\s]','',text)

    #Tokenize the text
    text = word_tokenize(text)

    return text

tess_df.head()

tess_df['Text'] = tess_df.Text.apply(CleanText)
tess_df['Text'] = tess_df.Text.apply(lambda text: " ".join(w for w in text if w not in stop_words))

tess_df.head()

#Building Model
#Model Selection and Validation
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
import pickle

X = tess_df['Text']
y = tess_df['Class']
Z = predict_df['Text']

X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2,random_state=2)

pipeline = Pipeline([
    ('bow',CountVectorizer()),  # strings to token integer counts, analyzer=text_processing
    ('tfidf', TfidfTransformer()),  # integer counts to weighted TF-IDF scores
    ('classifier', MultinomialNB()),  # train on TF-IDF vectors w/ Naive Bayes classifier
])
# Fit the model on training set
pipeline.fit(X_train, y_train)
# save the model to disk
filename = 'finalized_model.pkl'
pickle.dump(pipeline, open(filename, 'wb'))


#Generate prediction on our test data
predictions = pipeline.predict(X_test)
print("Accuracy is: ",accuracy_score(predictions,y_test))