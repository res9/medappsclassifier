import glob
import os
import re
from glob import glob
import csv

import pandas as pd
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer



csv_file = 'Groundtruthxml.csv'


xml_df = pd.read_csv(csv_file,names=['Item_Name','XML','Label'],sep=',', encoding='utf-8')


print(xml_df)







def CleanText(text):
    #Lower the text

    global app_count
    print("cleaning....")
    print(app_count)
    if app_count==65 or app_count==354 or app_count==431:
        app_count += 1
        return text
    try:
        text = text.lower()
    except:
        print('ignoring text......')

    #Remove all punctuation
    try:
        text = re.sub(r'[^\w\s]','',text)
    except:
        print('ignoring text....')

    #Tokenize the text
    text = word_tokenize(str(text))

    app_count+=1

    return text



xml_df['XML'] = xml_df.XML.apply(CleanText)
xml_df['XML'] = xml_df.XML.apply(lambda text: " ".join(w for w in text if w not in stop_words))








#Building Model
#Model Selection and Validation
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
import pickle




###################Tesseract model#######################################

# X = tess_df['Text']
# y = tess_df['Class']
# Z=predict_df['Text']
# predict_files=predict_df['Item_name']
# X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2,random_state=2)
#
# pipeline = Pipeline([
#     ('bow',CountVectorizer()),  # strings to token integer counts, analyzer=text_processing
#     ('tfidf', TfidfTransformer()),  # integer counts to weighted TF-IDF scores
#     ('classifier', MultinomialNB()),  # train on TF-IDF vectors w/ Naive Bayes classifier
# ])
# # Fit the model on training set
# pipeline.fit(X_train, y_train)
# # save the model to disk
# filename = 'finalized_model.pkl'
# pickle.dump(pipeline, open(filename, 'wb'))
#
#
# #Generate prediction on our test data
# predictions = pipeline.predict(X_test)
# print("Accuracy is: ",accuracy_score(predictions,y_test))
#
# final_prediction=pipeline.predict(Z)
#
# i=0
# for p in final_prediction:
#     print(predict_files[i],p)
#     i+=1


# ############################## XML Prediction Model #################

X = xml_df['XML']
y = xml_df['Label']




X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2)

pipeline = Pipeline([
    ('bow', CountVectorizer()),  # strings to token integer counts, analyzer=text_processing
    ('tfidf', TfidfTransformer()),  # integer counts to weighted TF-IDF scores
    ('classifier', MultinomialNB()),  # train on TF-IDF vectors w/ Naive Bayes classifier
])
# Fit the model on training set
pipeline.fit(X_train, y_train)
# save the model to disk
filename = 'finalized_model.pkl'
pickle.dump(pipeline, open(filename, 'wb'))

# Generate prediction on our test data
predictions = pipeline.predict(X_test)
print("Accuracy is: ", accuracy_score(predictions, y_test))




