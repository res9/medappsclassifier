import glob
import os
import re
from glob import glob
import pandas as pd
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
import pickle


# Tesseractfunction
def createDataset(root_dir):
    dataset_list = []
    data_to_predict_list = []
    class_dirs = glob(root_dir + '/*')
    # Go through each sub_directory, which represents a different ground truth class
    for class_dir in class_dirs:
        #print(class_dir)
        if (os.path.isfile(class_dir)):
            with open(class_dir, 'r',encoding='utf-8') as f:
                file_text = f.read().replace('\n', '')
            data_to_predict_list.append([class_dir, file_text])
        for root, dirs, files in os.walk(class_dir):
            # For each file, read the text and append it to the dataset
            for file in files:
                print(class_dir)
                #print(file)
                with open(os.path.join(class_dir, file), 'r', encoding='utf-8') as f:
                        file_text = f.read().replace('\n','')
                dataset_list.append([file, file_text, class_dir.split(' ')[1]])
    # Create the pandas dataset
    tess_df = pd.DataFrame(dataset_list, columns=['Item_name', 'Text', 'Class'])
    predict_df = pd.DataFrame(data_to_predict_list, columns=['Item_name', 'Text'])
    return tess_df, predict_df


root_dir = 'tesseract1'
tess_df, predict_df = createDataset(root_dir)

tess_df.head()
predict_df.head()


def cleanText(text):
  text = text[0]
  # Lower the text
  try:
    text = text.lower()
  except:
    print("ignoring text")
  # Remove all punctuation
  try:
    text = re.sub(r'[^\w\s]', '', text)
  except:
      print("ignoring text")
  # Tokenize the text
  text = word_tokenize(text)
  return text


stop_words = stopwords.words('english')


tess_df['Text'] = tess_df.Text.apply(cleanText)
tess_df['Text'] = tess_df.Text.apply(lambda text: " ".join(w for w in text if w not in stop_words))

predict_df['Text'] = predict_df.Text.apply(cleanText)
predict_df['Text'] = predict_df.Text.apply(lambda text: " ".join(w for w in text if w not in stop_words))

tess_df.head()
predict_df.head()

print(tess_df)
print(predict_df)

# Building Model
# Model Selection and Validation


X = tess_df['Text']
y = tess_df['Class']
Z = predict_df['Text']

predict_files = predict_df['Item_name']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2)

pipeline = Pipeline([
    ('bow', CountVectorizer()),  # strings to token integer counts, analyzer=text_processing
    ('tfidf', TfidfTransformer()),  # integer counts to weighted TF-IDF scores
    ('classifier', MultinomialNB()),  # train on TF-IDF vectors w/ Naive Bayes classifier
])
# Fit the model on training set
pipeline.fit(X_train, y_train)
# save the model to disk
filename = 'finalized_model.pkl'
pickle.dump(pipeline, open(filename, 'wb'))

# Generate prediction on our test data
predictions = pipeline.predict(X_test)
print("Accuracy is: ", accuracy_score(predictions, y_test))

final_prediction = pipeline.predict(Z)

i = 0
for p in final_prediction:
    print(predict_files[i], p)
    i += 1

