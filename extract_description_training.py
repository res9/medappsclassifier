from sys import stdin, stdout
from json import load
from string import punctuation


def sanitize_line(line):
    for _ in punctuation:
        line = line.replace(_, ' ')
    words = line.lower().split()
    to_remove = [] # screw nltk
    return ' '.join(_ for _ in words if _ not in to_remove)


def sanitize_item_name(lines):
    return ' '.join(sanitize_line(_) for _ in lines)


def sanitize_description(lines):
    return ' '.join(sanitize_line(_) for _ in lines)


def sanitize_label(line):
    return line


def csv_line_from_json_element(element, field_sanitizers):
    return ','.join([func(element[name]) for (name, func) in field_sanitizers])


def main(*, source, destination, field_sanitizers):
    destination.write(','.join(name for (name, _) in field_sanitizers))
    destination.write('\n')
    for element in load(source):
        destination.write(csv_line_from_json_element(element, field_sanitizers))
        destination.write('\n')


if __name__ == '__main__':

    main(
        source=stdin,
        destination=stdout,
        field_sanitizers=(
            ('Item_name', lambda _: sanitize_item_name(_)),
            ('Description', lambda _: sanitize_description(_)),
            ('label', lambda _: sanitize_label(_)),
        )
    )