import glob
import os
import re
from glob import glob
from typing import Tuple

import pandas as pd
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
import pickle
import json
import pprint
from string import punctuation


stop_words = stopwords.words('english')
def CleanText(text):
    for _ in punctuation:
        text = text.replace(_, ' ')
    return ' '.join(word_tokenize(text.lower()))


# Description

with open('output.labeled.json') as f:
    training_desc = json.load(f)

from collections import namedtuple

Item = namedtuple('Item', ['name', 'description', 'label'])


def sanitize_line(line):
    if any(word.upper() in line.upper() for word in stop_words):
        return ''
    else:
        return CleanText(line)


def extract_description(lines):
    return ",".join([sanitize_line(_) for _ in lines])


def extract_item(element):
    return Item(
        description=extract_description(element['Description']),
        name=element['Item_name'],
        label=element['label']
    )


items = tuple(extract_item(_) for _ in training_desc)

descrip_df = items


with open('output.unlabeled.json') as g:
    predict_desc = json.load(g)

Itema = namedtuple('Itema', ['name', 'description'])


def extract_item1(element):
    return Itema(
        description=extract_description(element['Description']),
        name=element['Item_name'],
    )


items1 = tuple(extract_item1(_) for _ in predict_desc)
predict_descripdf = items1


X = descrip_df.description
y = descrip_df.label
Z = predict_descripdf.description

print(Z)
predict_namexml = predict_descripdf.Item_name
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2)

pipeline = Pipeline([
    ('bow', CountVectorizer()),  # strings to token integer counts, analyzer=text_processing
    ('tfidf', TfidfTransformer()),  # integer counts to weighted TF-IDF scores
    ('classifier', MultinomialNB()),  # train on TF-IDF vectors w/ Naive Bayes classifier
])
# Fit the model on training set
pipeline.fit(X_train, y_train)
# save the model to disk
filename = 'finalized_model.pkl'
pickle.dump(pipeline, open(filename, 'wb'))

# Generate prediction on our test data
predictions = pipeline.predict(X_test)
print("Accuracy is: ", accuracy_score(predictions, y_test))

final_prediction_xml = pipeline.predict(Z.values.astype('U'))

i = 0
for p in final_prediction_xml:
    print(predict_namexml[i], p)
    i += 1


