_all_ = ['get_pipeline', 'clean_text']

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from string import punctuation


def get_pipeline():
    return Pipeline([
        ('bow', CountVectorizer()),  # strings to token integer counts, analyzer=text_processing
        ('tfidf', TfidfTransformer()),  # integer counts to weighted TF-IDF scores
        ('classifier', MultinomialNB()),  # train on TF-IDF vectors w/ Naive Bayes classifier
    ])


def clean_text(text):
    for _ in punctuation:
        text = text.replace(_, ' ')
    tokens = word_tokenize(text.lower())
    to_remove = {_.lower() for _ in stopwords.words('english')}  # lazy me
    return ' '.join(_ for _ in tokens if _ not in to_remove)
