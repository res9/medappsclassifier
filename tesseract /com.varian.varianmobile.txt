 
VARIAN MOBILE™ APPLICATION

END USER LICENSE AGREEMENT
The Varian Mobile™ application (the "App") is made
available through an application store, such as the Apple
App Store and Google Play (collectively "Distributors").
As used herein, "you" and "your" refer to the individual or
entity that wishes to use the App and “our” refers to
Varian Medical Systems, Inc. ("Licensor"). The App is
licensed, not sold, to you. Your License (as defined
below) to the App is subject to your prior acceptance of
this Varian Mobile™ Application End User License
Agreement (“EULA”), and your agreement that the terms
of the EULA will apply to your use of the App. Your
License to the App is granted by Licensor. Licensor
reserves all ownership and intellectual property rights in
and to the App. Please note, Licensor’s products and
services are diverse, and you may therefore be subject to
additional terms, policies, product or service requirements
that may apply when you access and use the App or
follow links that may take you to another Licensor site.
CLICKING ON THE "I AGREE" BUTTON constitutes
acceptance of the EULA.

The App supports the iPhone 5s® and above with iOS 9.x
and above and Android™ 6.x and above (each an
“Approved Device”), and enables you to access, review
and use certain data (and perform certain actions and
functions with such data) where such data is provided by

Me_147351310_1
any applicable laws, including but not limited to
applicable local laws. The Licensor, and its licensors,
reserve the right to change, suspend, remove, or disable
access to any Services or Third-Party Materials at any
time without notice. In no event will the Licensor be
liable for the removal of or disabling of access to any
such Services or Third-Party Materials. The Licensor may
also impose limits on the use of or access to certain
Services or Third-Party Materials, in any case and without
notice or liability.

Licensor's technical support organization may provide
technical support for the App directly to Customer under
a separate agreement, but Licensor does not provide
technical support to you or any end user. Licensor and
you acknowledge that Distributors have no obligation
whatsoever to furnish any maintenance and support
services with respect to the App.

e. No Warranty. YOU EXPRESSLY ACKNOWLEDGE
AND AGREE THAT USE OF THE APP AND YOUR
RELIANCE ON THE OPERATION, OUTPUT OR
RESULTS OF THE APP IS AT YOUR SOLE RISK
AND THAT THE ENTIRE RISK AS TO
SATISFACTORY QUALITY, PERFORMANCE,
ACCURACY, AVAILABILITY AND EFFORT IS
WITH YOU. TO THE MAXIMUM EXTENT

Me_147351310_1
 

 
ia
 
sublicensable, freely-revocable, limited license to run the
App on your Approved Device solely for the purpose of
allowing you to view and interact with Customer’s
ARIA® oncology information system via the Licensor’s
Subscription Service (“License”). The License is subject
to your accessing the App on an Approved Device that
you own and/or control. The License is personal to you
and you are prohibited from distributing or making the
App available to any third-party and/or broadcasting it
over a network where it could be used by multiple devices
at the same time. The License is further subject to your
use being made solely as permitted by the Distributors’
usage rules as set forth in their app store terms and
conditions. You are prohibited from renting, leasing,
lending, selling, redistributing or sublicensing the App.
You may not copy (except as expressly permitted by the
License), decompile, reverse engineer, disassemble,
attempt to derive the source code of, modify, or create
derivative works from the App, any updates, or any part
thereof, and any attempt to do the foregoing is a material
violation of the rights of the Licensor and its licensors.
You understand and agree that if you breach the License
or any restrictions as included herein in the EULA, you
may be subject to prosecution and damages. You are not
permitted to use the App for any purpose other than as
expressly permitted under the EULA. You acknowledge

Me_147351310_1
PERMITTED BY APPLICABLE LAW, THE APP AND
ANY SERVICES PERFORMED OR PROVIDED BY
THE APP ("APPLICATION SERVICES") ARE
PROVIDED "AS IS" AND "AS AVAILABLE", WITH
ALL FAULTS AND WITHOUT WARRANTY OF ANY
KIND, AND LICENSOR HEREBY DISCLAIMS, TO
THE MAXIMUM EXTENT PERMITTED BY LAW,
ALL WARRANTIES AND CONDITIONS WITH
RESPECT TO THE APP AND ANY APPLICATION
SERVICES, EITHER EXPRESS, IMPLIED OR
STATUTORY, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES AND/OR
CONDITIONS OF MERCHANTABILITY, OF
SATISFACTORY QUALITY, OF FITNESS FOR A
PARTICULAR PURPOSE, OF ACCURACY, OF
QUIET ENJOYMENT, AND NON-INFRINGEMENT
OF THIRD-PARTY RIGHTS. LICENSOR DOES NOT
WARRANT AGAINST INTERFERENCE WITH YOUR
ENJOYMENT OF THE APP, THAT THE FUNCTIONS
CONTAINED IN, OR APPLICATION SERVICES
PERFORMED OR PROVIDED BY, THE APP WILL
MEET YOUR REQUIREMENTS, THAT THE
OPERATION OF THE APP OR APPLICATION
SERVICES WILL BE UNINTERRUPTED OR ERROR-
FREE, OR THAT DEFECTS IN THE APP OR
APPLICATION SERVICES WILL BE CORRECTED.

Me_147351310_1
POSSIBILITY OF SUCH DAMAGES. SOME
JURISDICTIONS DO NOT ALLOW THE

LIMITATION OF LIABILITY FOR PERSONAL
INJURY, OR OF INCIDENTAL OR
CONSEQUENTIAL DAMAGES, SO THIS
LIMITATION MAY NOT APPLY TO YOU. In no event
shall Licensor's total aggregate liability to you for
damages exceed the amount of fifty dollars ($50.00). The
foregoing limitations will apply even if the above stated
remedy fails of its essential purpose.

g. Export Restrictions. You acknowledge that you are
responsible for compliance with applicable export laws
and restrictions. You may not use or otherwise export or
re-export the App except as authorized by United States
law and the laws of the jurisdiction in which the App was
obtained or any jurisdictions to which it is later
transferred. In particular, but without limitation, the App
may not be exported or re-exported (1) into any
embargoed destinations (i.e. for the US: Iran, Syria, Cuba,
North Korea, and the Crimea — as may be updated from
time to time by the U.S., and note that export restrictions
could also apply to other destinations), (2) to any person
or entity on the U.S. Treasury Department's list of
Specially Designated Nationals or the U.S. Department of
Commerce Denied Person's List or Entity List, or (3) in

Me_147351310_1
 
Til

www.MyVarian.com
 
Varidn)
 

 
EXPRESSLY CONSENT TO THE FOREGOING
COLLECTION, STORAGE, PROCESSING,
MAINTENANCE, UPLOADING, SYNCING,
TRANSMITTING, SHARING, OR DISCLOSURE OF
USER DATA. By continuing to use the App, you indicate
your continued consent to such collection, storage,
processing, maintenance, uploading, syncing,
transmitting, sharing, or disclosure of User Data.

c. Termination. The EULA is effective until terminated by
you and/or Licensor. Licensor may terminate the EULA
and use of the App if you fail to comply with any terms of
the EULA or the applicable Distributor’s terms of use, as
applicable. The EULA will terminate automatically upon
the termination of the Licensor’s Subscription Service or
upon termination of your employment and/or affiliation
with Customer. Upon termination of the EULA, you shall
cease all use of the App, and destroy all copies, full or
partial, of the App.

d. Services; Third-Party Materials. The App may enable
access to Licensor's and third parties’ services and
websites (collectively and individually, "Services"). Use
of the Services may require Internet access and that you
accept additional third-party terms of service.

Certain Services may display, include or make available
content, data, information, applications or materials from

ME_147351310_1
 
 
 
 
or by any means. You agree not to modify, rent, lease,
loan, sell, distribute, or create derivative works based on
the Services, in any manner, and you shall not exploit the
Services in any unauthorized way whatsoever, including
but not limited to, by trespass or burdening network
capacity. You further agree not to use the Services in any
manner to harass, abuse, stalk, threaten, defame or
otherwise infringe or violate the rights of any other party,
and that the Licensor is not in any way responsible for
any such use by you, nor for any harassing, threatening,
defamatory, offensive or illegal messages or
transmissions that you may receive as a result of using
any of the Services. You agree to comply with any
applicable third-party terms of agreement when using the
App, e.g., you must not be in violation of a wireless data
service agreement when using the App.

In addition, third-party Services and Third-Party
Materials that may be accessed from, displayed on or
linked to or from Approved Devices may not be available
in all languages or in all countries. The Licensor makes
no representation that such Services and Third-Party
Materials are appropriate or available for use in any
particular location. To the extent you choose to access
such Services or Third-Party Materials, you do so at your
own initiative and are responsible for compliance with

Me_147351310_1
i. Licensor. The Licensor of the App is Varian Medical
Systems, Inc. and is located at: 3100 Hansen Way, Palo
Alto, CA 94304. You acknowledge that Licensor does not
provide technical support to you or any end user and your
support, if any, for the use of the App would be provided
directly to you by Customer. You may contact Licensor
with any questions, complaints or claims with respect to
the App at 1-888-827-4265 or at support @varian.com.

j. Third-party beneficiary. You acknowledge and agree
that Distributors and its subsidiaries are third-party
beneficiaries to the EULA, and that, upon your
acceptance of the EULA, Distributors will have the right
(and will be deemed to have accepted the right) to enforce
the terms of the EULA against you as a third-party
beneficiary thereof.

k. Choice of law; venue. The laws of the State of
California, excluding its conflicts of law rules, govern the
EULA, its construction, and your use of the App. Any
action arising under the EULA shall be brought
exclusively in California, and you and Licensor expressly
consent to the personal jurisdiction of the state and federal
courts located in the county of Santa Clara, California for
such purposes. Your use of the App may also be subject
to other local, state, national, or international laws.

ME_147351310_1
technologies. For the purposes of this EULA, Licensor
will collect your first and last name, and your email
address when you sign in to use the App. You
acknowledge that use of the App may result in User Data
being transmitted between your device and a database
server designated by Customer and that such User Data
may be collected, stored, processed, maintained and used
by Customer. More specifically, but without limiting the
foregoing, you acknowledge that: (1) information
regarding the hardware model and operating system
version of the device on which you are running the App
may be collected, transmitted to and stored on a database
server designated by Customer, may be transmitted to
Licensor and may be used to make changes, updates or
improvements to or to optimize the performance of the
App or to otherwise inform future development; and (2)
audit logs reflecting your logins, logouts and the activities
you have accessed through your use of the App may be
generated in connection with your use of the App and
may be collected, transmitted and stored on a database
server designated by Customer and may be made
available to Customer and may also be made available to
Licensor for troubleshooting. Personal information
collected about you by Licensor pursuant to this EULA
may be held in the United States of America. BY
CLICKING ON THE "ACCEPT" BUTTON, YOU

Me_147351310_1
 
 

 

 

 

 

 

 
NO ORAL OR WRITTEN INFORMATION OR
ADVICE GIVEN BY LICENSOR, ITS AUTHORIZED
REPRESENTATIVE, OR CUSTOMER SHALL
CREATE A WARRANTY. SHOULD THE APP OR
APPLICATION SERVICES PROVE DEFECTIVE,
YOU ASSUME THE ENTIRE COST OF ALL
NECESSARY SERVICING, REPAIR OR
CORRECTION. SOME JURISDICTIONS DO NOT
ALLOW THE EXCLUSION OF IMPLIED
WARRANTIES OR LIMITATIONS ON APPLICABLE
STATUTORY RIGHTS OF A CONSUMER, SO THE
ABOVE EXCLUSION AND LIMITATIONS MAY NOT
APPLY TO YOU. IN THE EVENT OF ANY FAILURE
OF THE APP TO CONFORM TO ANY APPLICABLE
WARRANTY, YOU MAY NOTIFY DISTRIBUTORS,
AND DISTRIBUTORS, IN THEIR SOLE
DISCRETION, MAY REFUND THE PURCHASE
PRICE FOR THE APP TO YOU, IF ANY. TO THE
MAXIMUM EXTENT PERMITTED BY LAW,
DISTRIBUTORS WILL HAVE NO OTHER
WARRANTY OBLIGATION WHATSOEVER WITH
RESPECT TO THE APP. TO THE MAXIMUM
EXTENT PERMITTED BY LAW, DISTRIBUTORS
SHALL HAVE NO WARRANTY OBLIGATION
WHATSOEVER WITH RESPECT TO THE SERVICES
AND NO OBLIGATION TO ADDRESS ANY OF
YOUR CLAIMS OR CLAIMS OF ANY THIRD-PARTY
RELATING TO THE APP OR YOUR POSSESSION
AND/OR USE OF THE APP, INCLUDING, BUT NOT
LIMITED TO: (I) PRODUCT LIABILITY CLAIMS; (ID)
ANY CLAIM THAT THE APP FAILS TO CONFORM
TO ANY APPLICABLE LEGAL OR REGULATORY
REQUIREMENT; AND (II) CLAIMS ARISING
UNDER CONSUMER PROTECTION OR SIMILAR
LEGISLATION.

f. Limitation of Liability. TO THE MAXIMUM
EXTENT PERMITTED BY LAW, IN NO EVENT
SHALL LICENSOR BE LIABLE FOR PERSONAL
INJURY, OR ANY INCIDENTAL, SPECIAL,
INDIRECT OR CONSEQUENTIAL DAMAGES
WHATSOEVER, INCLUDING, WITHOUT
LIMITATION, DAMAGES FOR LOSS OF PROFITS,
LOST OR MODIFIED DATA, BUSINESS
INTERRUPTION OR ANY OTHER COMMERCIAL
DAMAGES OR LOSSES, ARISING OUT OF OR
RELATED TO YOUR USE OR INABILITY TO USE
THE APP, APPLICATION SERVICES, SERVICES, OR
THIRD-PARTY MATERIALS HOWEVER CAUSED,
REGARDLESS OF THE THEORY OF LIABILITY
(CONTRACT, TORT OR OTHERWISE) AND EVEN IF
LICENSOR HAS BEEN ADVISED OF THE
.
»
 

 
y N
that Licensor may freely audit your use of the App with or
without notice. You further agree that you will fully
comply with all applicable laws and any applicable terms
or conditions of Customer’s EULA in your use of the
App. The terms of the EULA will govern any upgrades
provided by Licensor that replace and/or supplement the
original App, unless such upgrade is accompanied by a
separate license in which case the terms of that license
will govern, and you agree to promptly install any such
upgrade and cease use of the prior version. Nothing in this
paragraph creates an obligation for Licensor to prepare or
provide any upgrades to the App.

b. Consent to Use of Data. You agree that Licensor and
Customer may collect, store, process, maintain, upload,
sync, transmit, share, disclose and use certain data and
information, including but not limited to information or
data regarding the characteristics or usage of your device,
system and application software, use of the App and
peripherals as well as personal information, user location
data and user content (collectively "User Data") to
facilitate the provision of services or functionality for the
App, including, but not limited to authentication,
performance optimization, software updates, product
support and other services (if any) related to the App to
improve Licensor's products or to provide services or

Me_147351310_1
third-parties including Customer ("Third-Party
Materials") or provide links to certain third-party
websites. By using the Services, you acknowledge and
agree that the Licensor is not responsible for examining or
evaluating the content, accuracy, completeness,
timeliness, validity, copyright compliance, legality,
decency, quality or any other aspect of such Third-Party
Materials or websites. The Licensor does not warrant or
endorse, and the Licensor does not assume and will not
have any liability or responsibility to you or any other
person for, any Services, Third-Party Materials or
websites, or any other materials, products, or services of
third parties. Third-Party Materials and links to other
websites are provided solely as a convenience to you.
Neither the Licensor, nor any of its content providers
guarantees the availability, accuracy, completeness,
reliability, or timeliness of any information or data
displayed by any Services or any Third-Party Materials.

You agree that the Services may contain proprietary
content, information and material that is protected by
applicable intellectual property and other laws, including
but not limited to copyright, and that you will not use
such proprietary content, information or materials in any
way whatsoever except for permitted use of the Services.
No portion of the Services may be reproduced in any form

Me_147351310_1
contravention of U.S. Export Administration Regulations
(also managed by the U.S. Department of Commerce). By
using the App, you represent and warrant that you are not
located in any such country or on any such list (unless
prior authorization has been obtained pursuant to U.S.
laws and regulations). You also agree that you will not
use the App for any purposes prohibited by U.S. law
and/or any applicable law, including, without limitation,
the development, design, manufacture or production of
nuclear, missiles, or chemical or biological weapons.

h. U.S. Government Users. The App and related
documentation are "Commercial Items", as that term is
defined at 48 C.F.R. §2.101, consisting of "Commercial
Computer Software" and "Commercial Computer
Software Documentation", as such terms are used in 48
C.F.R. §12.212 or 48 C.F.R. §227.7202, as applicable.
Consistent with 48 C.F.R. §12.212 or 48 C.F.R.
§227.7202-1 through 227.7202-4, as applicable, the
Commercial Computer Software and Commercial
Computer Software Documentation are being licensed to
U.S. Government end users (a) only as Commercial Items
and (b) with only those rights as are granted to all other
end users pursuant to the terms and conditions herein.
Unpublished-rights reserved under the copyright laws of
the United States.

Me_147351310_1
a connection to an instance of ARIA® oncology
information system (the “Licensor’s Subscription
Service”). The Licensor’s Subscription Service is a relay
service that connects the App to Customer’s (defined
below) ARIA® oncology information system and the App
provides the ability to view and interact with patient chart
information from Customer’s ARIA® oncology
information system on a mobile device. The Licensor’s
Subscription Service is a licensed service sold by
Licensor to our customers (typically, a medical care
provider) (“Customer”) with each such entity, at the time
of your access, under a separate agreement.

To log in to the App, you must successfully enter the
unique and confidential username and password issued to
you by Customer (“Your Login Credentials”). You
acknowledge that Customer may, in its sole discretion: (1)
issue and/or terminate Your Login Credentials; and (2)
require you to enter into a separate agreement and/or
agree to certain policies (including any applicable privacy
policies of Customer) as a condition of providing you
with access to the Licensor’s Subscription Service
through the App (collectively “Customer’s EULA”).

a. Scope of License. Provided that you comply with your
obligations under the EULA, App access is granted by
Licensor under this non-exclusive, non-transferable, non-

ME_147351310_1
 

    
C
 
 
 
 
 
Cl
G)
 
a

 
 
y

=

y
 
 
y

=

y
 
 
 
a
 
 
 
 
