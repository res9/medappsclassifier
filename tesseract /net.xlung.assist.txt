Name
Gender

Age (years)
Height (cm)
Altitude (m)

Diagnoses

John Silva

Male Female

29

170

COPD
 

PBW and VT setting

 

MECHANICS

Airway Resistance (Raw)
Static Compliance (Cst)
Driving pressure

Time constant

 

GAS EXCHANGE
FIO, setting for PaO,

RR setting for PaCO,

 
rrr PPP

pH < 7.20

Severe acidosis

PaO, < 60 mmHg (8 kPa)
Severe hypoxemia

VT PBW (ml/kg) < 4

Risk of atelectasis

Tinsp <0.5s
Tinsp too short, risk of asynchrony

PaCQ, < 30 mmHg
Severe hypocapnia

SpO, < 90%
Severe hypoxemia
6/16/15 3:20 AM

6 alerts

6/16/15 3:49 AM

6 alerts

6/21/15 6:52 PM

6 alerts
 
G)
Nome

Sexo

Idade (anos)
Altura (cm)
Altitude (m)

Diagnostico(s)

Joao Silva

Masculino Feminino

29

170

DPOC
rrr PrP PEP

pH < 7.20

Acidose grave

PaO, < 60 mmHg (8 kPa)
Hipoxemia severa

VC Peso Ideal (ml/kg) < 4

Risco de atelectasia

Tinsp < 0,5 s
Tinsp muito curto, risco de assincronia

PaCQ, < 30 mmHg
Hipocapnia severa

SpO, < 90%
Hipoxemia severa
16/06/15 03:20

6 alertas

16/06/15 03:49

6 alertas

21/06/15 18:52

6 alertas
 

Peso Ideal e Ajuste do VC

 

MECANICA RESPIRATORIA
Resisténcia das Vias Aéreas (Rva)
Complacéncia Estatica (Cst)
Driving pressure

Constante de tempo

 

TROCA GASOSA
FIO, para PaO, alvo

Freq. resp. para PaCO, alvo
