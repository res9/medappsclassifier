 
 

Histology of Blood and Bone Marrow —
A Second! 6ok™ Series

Jump To Slide

ULES A UMUC ce)
WT Cer THT}

OPP a RS cmon

EAE ed ry

 

 

 
Review Topics

  
  
 
  
 
 

@ Blood and Bone Marrow
Bone and Bone Formation
Cardiovascular System

eo Cartilage

@ Cell Biology
Connective Tissue

Central Nervous System

(0) (o)
rv All

 
eo ———————

Central Nervous System

© ®
All rv
»

 
   

@ help &
Review Topics About

» Register
Blood and Bone Marrow

@ orientation

Bone and Bone Formation
Lock

Cardiovascular System a
Cartilage

Cell Biology

Connective Tissue

Central Nervous System

 
 

 

Histology of Blood and Bone Marrow —
A SecondL6ok™ Series

Michael Hortsch, P’
Department nd Develo! Biology
“ iD

 
     
   

  

y of Mic
LOW

Pe ET
Yai)

COPPA Cau Renee aene Uy

ae ed ry

 
 

 
Briefly summarize the most important
functions of this cell fragment?

Pere) @o t
 

 

Histology of Blood and Bone Marrow —
A SecondL6ok™ Series

Department > Cell ‘ -_. Biology

Taree Michigan
hortsch( Talen=ye [0]

    
   
 

n

PUPS eRe eed ene

ae ed ry

 
Po
MBecondlL60ok" ~~
Histology Lite —
A SecondL6ok™ Series

Michael Hortsch, Ph.D.
Department of Cell and Developmental Biology
University of Michigan
hortsch@umich.edu

®
University of Michigan
Medical School

© 2016 Regents of the University of Michigan
removal of debris,
such as dead cells and
pathogens, and antigen
feyKec alt=
Where are the stem cells
located that produce these
 
 

 
Identify and
name the four

layers indicated
YAU Renee lion
a
Identify the
spaces and

structures
indicated by
the lines and
Tae) Wice

 
Identify the
spaces and

ita U Leite goss)
indicated by
the lines and

arrows.

 
Clelland

Cd
Pd
)
i
Kf Screven
ty
Leeman
1.)
aS

line

0
ie y

Pee
 
Identify these cellular structures
and know where on epithelial cells
these structures are usually found.

 
oe eee ==ag
}

a

Pee
 
 
ra (Note the large cell
Dee
eC}

How abundant (in
Pee ae Td
ae Maroiuarelnelle\ere]
Maes aig
8-9%
Briefly summarize the most important functions of this cell type ?
Melee a relare Roa -aueueu else ater ROR US rN
prep tsen_brhealnenaren | aon oy oe prteehedee

transform into macrophages to aid with antigen-presentation.
 
 
ert

a
HH

ze
  
  

ae soe CUU eestor
ce a of a lymph node that
reece 1) Aa mE=xe}
eo
 
Lymphocytes can enter the lymph node sinuses via afferent lymphatic
vessels or from the bloodstream by crossing the cuboidal endothelium of
the high endothelial venules in the deep cortex of the lymph node.
 
 

 
leant

| aeen |

Asingle sercomere extends from one Z line to a neighboring Z line.

Name the structures indicated by the blue brackets and
green arrows and their major protein content.
| amend te cee tiaeel |

ey
Oe Pe

PET Koll Tin Rae ean}
~f——

DO  )
H band (contains only thick myosin filaments)
i ai

Aband (contains the thick myosin filaments
Ee lale Ra RSelcE-IK-e- oe eon alte am UN crn)
aaa
is

eee lioness

Indicate the borders of a full sarcomere in this EM micrograph.
Asingle sercomere extends from one Z line to a neighboring Z line.
I band (contains the thin actin filaments)

Pat aol ake
CT Rue ue ee EL eet]
The white dashed line

 
aint)

 

fn
ar
ty)
co
Es
n
ci)
S)
a
S)
l
o
An isogenous group of
chondrocytes
What is the developmental
iol sialle}
within an isogenous group?
 
 
oT}
What is the “hole” in
the middle called?
Haversian canal
(or osteonic canal)

Name histological
Pelee tee
Haversian canal.
 
 
Hypodermis
(or subcutaneous fascia)
 
 
Shown are remnants of a tooth in
a demineralized tissue section

Due to the demineralization,
which part of the tooth is not
visible in this micrograph?
 
 
 
 
Stomach
Indicate the depth of the gastric pits and the gastric glands for each image.
ae
Cardiac region

rel) ero
 
 
el Ta AVA AL MeCN Ue ULC ULE e
Ele are (eri Tonalin Veet Teme CeCe
Intestinal glands (Crypts of Lieberkithn)
 
ICTR Tier
epithelium (urothelium, ureter)
Yell ence RCN)
Identify the
Niests\cd SM Uae 1M LCR
Mela cXOM OVATE
colored arrows.

 
Identify the
vessels that are
marked by the
colored arrows.

 
ea
 

© 2012 Regents of 9
 

© 2012 Regents of
 
 
 

 

In the micrograph on the left,
indicate the layers/regions
where the following
hormones are produced.

Catecholamines
In the micrograph on the left,
indicate the layers/regions
where the following
hormones are produced.

Catecholamines

 
Re aS RU AM l R ey
in the quantity of hormone production)
 
 
Identify the cells marked by red asierisks.

LS
Identify the cells marked by white asterisks.
A at m
% 7 x 4 8
&& ms)
iauilNg
ES ol Taare 1CeleN Koay

el Taha eee alee oR aU eel Ae
iy

ry

Early and late
Macrophages (Note the large
eee con
free floating cells, here in a sinus
Eu amore)
ame the space
marked by the

 
Name the space
marked by the

 
They form the
corona radiata and
after ovulation will
Oral Radiology Lite -
A SecondL6Ok™ Series

Jacob Fischer jmfisch@umich.edu

Anna Caplis acaps@umich.edu
Erika Benavides, D.D.S., Ph.D. benavid@umich.edu

Department of Periodontics and Oral Medicine,
School of Dentistry, University of Michigan

Editor: Michael Hortsch, Ph.D., Departments of Cell and Dev. Biology and
of Learning Health Sciences, University of Michigan Medical School

co)
UTS CL
OP enh muCRO nr omVeniecuy
 
