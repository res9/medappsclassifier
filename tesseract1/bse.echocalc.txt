The British Society of Echocardiography (BSE) exists to promote the use and advancement of high quality ultrasound imaging in cardiology. It represents the interests of those working in clinical echocardiography at all levels and in all areas, including adult and paediatric cardiology, cardiovascular research and teaching. The BSE is a Registered Charity and is administered by a President and an elected Council. Membership is open internationally to everyone associated with echocardiography. Members are required to subscribe to the BSE Rules and Code of Professional Conduct.LV size & function
LV diastolic function
LV mass
Aortic dimensions
RV size & function
Left atrium
Right atrium
Dobutamine stress echoAndroid Echocalc v1.4
Android Echocalc is copyright Dr Allan Harkness 2011-14

Please email any comments, feedback or suggestions regarding the Android EchoCalc to:
Android@bsecho.org
You can email a bug direct from the app, with a screen-shot, by pressing the menu key then "Email a bug"

Graphics are based on originals by Dr Peter TK Milton

Android Echocalc is based on the original App for the iPhone by Dr Daniel Knight, Dr Kevin O'Gallagher & Mr Ciaran O'GallagherMedical Editor & Medical Application Management:
Dr Allan Harkness MBChB FRCP

Application Technical Management & Development:
Dr Allan Harkness MBChB FRCP

Medical Graphic Design & Illustration:
Dr Peter TK Milton BSc (Hons) MBBS

Based on the original App for the iPhone by:
Dr Daniel Knight BSc (Hons) MBBS MRCP
Dr Kevin O'Gallagher BA (Hons) MBBS MRCP
Mr Ciaran O'Gallagher MEng (Hons)

On behalf of the British Society of Echocardiography (BSE) Education Committee.Warning: this application ("Echocalc") is only for use by fully qualified echocardiographers. You expressly acknowledge and agree that use of the Application is at your sole risk and that the entire risk as to satisfactory quality, performance, accuracy and effort is with you. To the maximum extent permitted by applicable law the Application and any services performed or provided by the Application ("Services") are provided "as is" and "as available as", with all faults and without warranty of any kind. The BSE Application Development Team hereby disclaims all warranties and conditions in respect of the Application including but not limited to the implied warranties and/or conditions with merchantability, of satisfactory quality, of fitness for a particular purpose and of accuracy to the extent permitted by law, the BSE Application Development Team shall have no liability for the breach of contract or any incidental, special, indirect or consequential damages of any nature provided that nothing shall limit the BSE Application Development Team's liability for personal injury or death caused by the BSE Application Development Team's negligence.Aortic regurgitation
Aortic stenosis
Mitral regurgitation
Mitral stenosis
Pulmonary regurgitation
Pulmonary stenosis
Tricuspid regurgitation
Tricuspid stenosis
Prosthetic aortic valves
Prosthetic mitral valves