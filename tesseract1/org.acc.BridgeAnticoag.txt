a

 
 
G)
AV EVE Tecerg WIL eeelst=4

ENTE
Rearmed
CARDIOLOGY

  
ManageAnticoag

PUVA UO
COLLEGE of
CARDIOLOGY

 
ManageAnticoag

PUY eae COUN
COLLEGE of
CARDIOLOGY

 
ManageAnticoag

Pa erty

  
ManageAnticoag

AMERICAN
COLLEGE of
CARDIOLOGY

 
ManageAnticoag

UE UCO7 NN
|) COLLEGE 9,
CARDIOLOGY

   
ManageAnticoag

Re
 

ManageAnticoag

AMERICAN
;) COLLEGE of
7 CARDIOLOGY

 
ne
ne
re
ne
ne
ne
ne
ne
ne
ne
ne

a

ne

nn
en
OO
~
OOOO
TS
—
OOOO
OOOO
OOOO

—

OOOO

_
_—
ee
a
ee
ee
ee
—
ee
ee
ee
a
a

—

ne

—
—
7
-
OU
—
—
-
-

7

OU

7
_—
re
re
re
re
ee
re
re

BS

Bn

ee
OO
 

 

Gradel
EROA < 0.2 cm?
RVol< 30 mi
RF <30%

 

 
 

 

Grade IV
EROA 2 0.4 cm?
RVol2 60 ml
RF 250%

 

 
INR

4F-PCC Dose

 

INR<2
INR22and<4
INR4-6
INR>6
INR Unavailable

AF-PCC Not approved; use clinical judgement.
Administer 25 units/kg*
Administer 35 units/kg*
Administer 50 units/kg*

AF-PCC low fixed-dose option:
¢ 1000 units for any major bleed

e 1500 units for intracranial hemorrhage
Unknown
 

 

Grade lll
EROA 0.30-0.39 cm?
RVol 45-59 ml
RF 40-49%

 

 
BS

 
Po
Bs

ane
ieee
sete eete

 
OB

Oo
Oo
OS

 
 
 
 
b <eee
 
 
 
 

Beal ay ay ay a Ra aes

“a

SR epee

roe
os yor

SSA Sse eco SSNS
A ck tac i ehh ct
retin nto nt nt at tnt he
Be Re et a
ON
SSSSSSSS SS
SE cece chee ee
A ck tac i ehh ct
retin nto nt nt at tnt he
Be Re et a
retin nto nt nt at tnt he
A ck tac i ehh ct
ere ay
Be Re et a
Re
eave neat

a
ce
ce
S
ce
=
ce
=
ce
=
ce
=
ce
=
ce
me
=

virion nari
Sanaa

ere

fa
“a
is
“a
is
“a
is
“a
is
ss

ee
is
“a
is
“a
is
“a
S
is
 

 

Grade ll
EROA 0.2-0.29 cm?
RVol 30-44 ml
RF 30-39%

 

 
 

 

 

Assessment
O Confirm NYHA status
Q Confirm prohibitive surgical risk

 

 

 

 

 

 

O Characterize mechanism and location of MR
Q Identify presence and location of calcium
Q Measure flail width, flail gap, and grasping zone length
Q Confirm mitral area valve is > 4.0cm?

 

 

 

"Note: Transcatheter edge-to-edge clip repair not approved in US for patients with functional MR

Abbreviations: MR = mtralreguriatin; NYHA=New York Heart Assocation; TEE= transesophageal echecardography; TE =transthoraic echocardiography
@) ACC | Statin Intolerance
 
 
 
 
 
 
ne
ne
re
ne
ne
ne
ne
ne
ne
ne
ne

a

ne

nn
en
OO
~
OOOO
TS
—
OOOO
OOOO
OOOO

—

OOOO

_
_—
ee
a
ee
ee
ee
—
ee
ee
ee
a
a

—

ne

—
—
7
-
OU
—
—
-
-

7

OU

7
_—
re
re
re
re
ee
re
re

BS

Bn

ee
OO
 

 

Gradel
EROA < 0.2 cm?
RVol< 30 mi
RF <30%

 

 
 

 

Grade IV
EROA 2 0.4 cm?
RVol2 60 ml
RF 250%

 

 
CHF: Congestive Heart Failure; TIA: Transient ischemic attack

Indication

Patient characteristics

 

Mechanical valve prosthesis

Atrial fibrillation (AF)

Venous thromboembolism (VTE)

Prior thromboembolism with interruption of anticoagulation
Left ventricular or left atrial thrombus

Left ventricular assist device (LVAD)

Mechanical aortic valve + additional thrombotic
considerations: AF, CHF, prior stroke/TIA
Caged-ball or tilting disc aortic valve prosthesis
Stroke/TIA within 6 months

AF with CHADS, score of 2 4 (or CHA2DS2-VASc score of 2 6)
(84)

Stroke/ TIA within 3 months

Stroke risk = 10% per year

Rheumatic valve disease or mitral stenosis

VTE within 3 months
History of unprovoked or recurrent VTE
Active cancer and history of cancer-associated VTE

Any

Any

Any
Unknown
 

 

Grade lll
EROA 0.30-0.39 cm?
RVol 45-59 ml
RF 40-49%

 

 
BS

 
Po
Bs

ane
ieee
sete eete

 
OB

Oo
Oo
OS

 
 
 
 
b <eee
 
 
 
 

Beal ay ay ay a Ra aes

“a

SR epee

roe
os yor

SSA Sse eco SSNS
A ck tac i ehh ct
retin nto nt nt at tnt he
Be Re et a
ON
SSSSSSSS SS
SE cece chee ee
A ck tac i ehh ct
retin nto nt nt at tnt he
Be Re et a
retin nto nt nt at tnt he
A ck tac i ehh ct
ere ay
Be Re et a
Re
eave neat

a
ce
ce
S
ce
=
ce
=
ce
=
ce
=
ce
=
ce
me
=

virion nari
Sanaa

ere

fa
“a
is
“a
is
“a
is
“a
is
ss

ee
is
“a
is
“a
is
“a
S
is
 

 

Grade ll
EROA 0.2-0.29 cm?
RVol 30-44 ml
RF 30-39%

 

 
 

 

 

Assessment
O Confirm NYHA status
Q Confirm prohibitive surgical risk

 

 

 

 

 

 

O Characterize mechanism and location of MR
Q Identify presence and location of calcium
Q Measure flail width, flail gap, and grasping zone length
Q Confirm mitral area valve is > 4.0cm?

 

 

 

"Note: Transcatheter edge-to-edge clip repair not approved in US for patients with functional MR

Abbreviations: MR = mtralreguriatin; NYHA=New York Heart Assocation; TEE= transesophageal echecardography; TE =transthoraic echocardiography
@) ACC | Statin Intolerance
 
 
 
 
 
 
Master branchne
ne
re
ne
ne
ne
ne
ne
ne
ne
ne

a

ne

nn
en
OO
~
OOOO
TS
—
OOOO
OOOO
OOOO

—

OOOO

_
_—
ee
a
ee
ee
ee
—
ee
ee
ee
a
a

—

ne

—
—
7
-
OU
—
—
-
-

7

OU

7
_—
re
re
re
re
ee
re
re

BS

Bn

ee
OO
 
 
 
b <eee
 
 
 

&) cotta PMAC

CARDIOLOGY __ Periprocedural Management of Anticoagulation

 
@) ACC | Statin Intolerance
 
 
 
 
 
 
 
ne
ne
re
ne
ne
ne
ne
ne
ne
ne
ne

a

ne

nn
en
OO
~
OOOO
TS
—
OOOO
OOOO
OOOO

—

OOOO

_
_—
ee
a
ee
ee
ee
—
ee
ee
ee
a
a

—

ne

—
—
7
-
OU
—
—
-
-

7

OU

7
_—
re
re
re
re
ee
re
re

BS

Bn

ee
OO
 
 
 
b <eee
 
 
 

Beal ay ay ay a Ra aes

“a

SR epee

roe
os yor

SSA Sse eco SSNS
A ck tac i ehh ct
retin nto nt nt at tnt he
Be Re et a
ON
SSSSSSSS SS
SE cece chee ee
A ck tac i ehh ct
retin nto nt nt at tnt he
Be Re et a
retin nto nt nt at tnt he
A ck tac i ehh ct
ere ay
Be Re et a
Re
eave neat

a
ce
ce
S
ce
=
ce
=
ce
=
ce
=
ce
=
ce
me
=

virion nari
Sanaa

ere

fa
“a
is
“a
is
“a
is
“a
is
ss

ee
is
“a
is
“a
is
“a
S
is
 
 
 
 
 
 
 
b <eee
