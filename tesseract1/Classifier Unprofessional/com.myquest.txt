{"states":[
	{
    	"code":"0",
        "name": "",
        "alpha-2": ""
    },
    {
    	"code":"1",
        "name": "Alabama",
        "alpha-2": "AL"
    },
    {
    	"code":"2",
        "name": "Alaska",
        "alpha-2": "AK"
    },
    {
    	"code":"3",
        "name": "Arizona",
        "alpha-2": "AZ"
    },
    {
    	"code":"4",
        "name": "Arkansas",
        "alpha-2": "AR"
    },
    {
    	"code":"5",
        "name": "California",
        "alpha-2": "CA"
    },
    {
    	"code":"6",
        "name": "Colorado",
        "alpha-2": "CO"
    },
    {
    	"code":"7",
        "name": "Connecticut",
        "alpha-2": "CT"
    },
    {
    	"code":"8",
        "name": "Delaware",
        "alpha-2": "DE"
    },
    {
    	"code":"9",
        "name": "District of Columbia",
        "alpha-2": "DC"
    },
    {
    	"code":"10",
        "name": "Florida",
        "alpha-2": "FL"
    },
    {
    	"code":"11",
        "name": "Georgia",
        "alpha-2": "GA"
    },
    {
    	"code":"12",
        "name": "Hawaii",
        "alpha-2": "HI"
    },
    {
    	"code":"13",
        "name": "Idaho",
        "alpha-2": "ID"
    },
    {
    	"code":"14",
        "name": "Illinois",
        "alpha-2": "IL"
    },
    {
    	"code":"15",
        "name": "Indiana",
        "alpha-2": "IN"
    },
    {
    	"code":"16",
        "name": "Iowa",
        "alpha-2": "IA"
    },
    {
    	"code":"17",
        "name": "Kansa",
        "alpha-2": "KS"
    },
    {
    	"code":"18",
        "name": "Kentucky",
        "alpha-2": "KY"
    },
    {
    	"code":"19",
        "name": "Lousiana",
        "alpha-2": "LA"
    },
    {
    	"code":"20",
        "name": "Maine",
        "alpha-2": "ME"
    },
    {
    	"code":"21",
        "name": "Maryland",
        "alpha-2": "MD"
    },
    {
    	"code":"22",
        "name": "Massachusetts",
        "alpha-2": "MA"
    },
    {
    	"code":"23",
        "name": "Michigan",
        "alpha-2": "MI"
    },
    {
    	"code":"24",
        "name": "Minnesota",
        "alpha-2": "MN"
    },
    {
    	"code":"25",
        "name": "Mississippi",
        "alpha-2": "MS"
    },
    {
    	"code":"26",
        "name": "Missouri",
        "alpha-2": "MO"
    },
    {
    	"code":"27",
        "name": "Montana",
        "alpha-2": "MT"
    },
    {
    	"code":"28",
        "name": "Nebraska",
        "alpha-2": "NE"
    },
    {
    	"code":"29",
        "name": "Nevada",
        "alpha-2": "NV"
    },
    {
    	"code":"30",
        "name": "New Hampshire",
        "alpha-2": "NH"
    },
    {
    	"code":"31",
        "name": "New Jersey",
        "alpha-2": "NJ"
    },
    {
    	"code":"32",
        "name": "New Mexico",
        "alpha-2": "NM"
    },
    {
    	"code":"33",
        "name": "New York",
        "alpha-2": "NY"
    },
    {
    	"code":"34",
        "name": "North Carolina",
        "alpha-2": "NC"
    },
    {
    	"code":"35",
        "name": "North Dakota",
        "alpha-2": "ND"
    },
    {
    	"code":"36",
        "name": "Ohio",
        "alpha-2": "OH"
    },
    {
    	"code":"37",
        "name": "Oklahoma",
        "alpha-2": "OK"
    },
    {
    	"code":"38",
        "name": "Oregon",
        "alpha-2": "OR"
    },
    {
    	"code":"39",
        "name": "Pennsylvania",
        "alpha-2": "PA"
    },
    {
    	"code":"40",
        "name": "Rhode Island",
        "alpha-2": "RI"
    },
    {
    	"code":"41",
        "name": "South Carolina",
        "alpha-2": "SC"
    },
    {
    	"code":"42",
        "name": "South Dakota",
        "alpha-2": "SD"
    },
    {
    	"code":"43",
        "name": "Tennessee",
        "alpha-2": "TN"
    },
    {
    	"code":"44",
        "name": "Texas",
        "alpha-2": "TX"
    },
    {
    	"code":"45",
        "name": "Utah",
        "alpha-2": "UT"
    },
    {
    	"code":"46",
        "name": "Vermont",
        "alpha-2": "VT"
    },
    {
    	"code":"47",
        "name": "Virginia",
        "alpha-2": "VA"
    },
    {
    	"code":"48",
        "name": "Washington",
        "alpha-2": "WA"
    },
    {
    	"code":"49",
        "name": "West Virginia",
        "alpha-2": "WV"
    },
    {
    	"code":"50",
        "name": "Wisconsin",
        "alpha-2": "WI"
    },
    {	
    	"code":"51",
        "name": "Wyoming",
        "alpha-2": "WY"
    },
     {	
    	"code":"52",
        "name": "",
        "alpha-2": ""
    }
]
}1.9.0
1.9.0
