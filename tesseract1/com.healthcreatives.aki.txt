6000

London Acute Kidney Injury Network
06000

London Acute Kidney Injury Network
0000

London Acute Kidney Injury Network
06000

London Acute Kidney Injury Network
6000

London Acute Kidney Injury Network
Acute Kidney Injury
06000

London Acute Kidney Injury Network
 
6000

London Acute Kidney Injury Network
 

 

0000

 

 
Acute Kidney Injury
 
 
 

 

 
a

oH 2!
30 =

 

i

 
 
 
 

( Obstetric AKI Pathway )

 

Institute in all cases with creatinine >90Ummol/L or serial creatinine rise
of 26ummol/L or 20ml/hr urine for 12 hours (if PET excluded)

THIS IS POTENTIALLY A MEDICAL EMERGENCY

Full set of physiological observations BP/HR/RR/SATS/TEMP
Assess for signs of shock/hypoperfusion- low BP/high HR/confusion/pale & cold skin
Review history and past results If MEOWS triggering- high flow oxygen, Review senior/HDU/ITU

Fluid therapy in AKI

If hypovolaemic give crystalloid 250ml. Followed by 125 ml/hr* Re-assess
Catheterise if obstruction and measure hourly urine output

Monitoring in AKI

Venous blood gas & lactate, U&E twice a day while creatinine rising
Fluid chart, regular fluid assessment and observations

Investigations in AKI

If proteinuria URGENT PCR
Ultrasound (obstruction),
Liver Profile, If low platelets blood film (fragmented RBC/PLT), LDH, Bilirubin, Reticulocytes

Supportive AKI care

Sepsis- ANTIBIOTICS within an hour. Review drug chart/thromboprophylaxis

Causes Think STOP AKI
Prerenal Sepsis/hypovolaemia (PPH)
Renal Toxicity NSAIDS, PET, HELLP, HUS,TTP
Postrenal Obstruction or ureteric damage during delivery

 

* Caution with PET

060 ©) website: www.londonaki.net
email: info@londonaki.net
Risk, Prevention and Recognition

 

 

 

 

Background

oe , Sepsis and hypoperfusion
Cardiac failure Si
Liver disease at eerireree

Diabetes Ff ,
Verraq poi REED Parenchymal kidney disease

Background nephrotoxic medications

 

 

 

 

AKI Develops

INSTITUTE CARE BUNDLE
Prevent AKI progression by rapid diagnosis, supportive care, specific therapy and appropriate referral

 
AKI Care Bundle

 

 

 

This is a Medical Emergency

 

 

 

General supportive care and escalation
Once euvolaemic give maintenance fluids (e.g. output plus 500mls),
fluid chart, daily weights, regular fluid assessment
Regular (at least 4 hourly) observations/NEWS with clear escalation plans
Review all drug dosages, consider proton pump inhibitor, consider dietetic review and nutrition

    

Urea, electrolytes, bone and venous bicarbonate at least daily, consider ABG
Monitor for complications, treat and escalate
Severe AKI (AKI 3) should be discussed nephrology and critical care regardless of cause
AKI Complications

Local Critical Care Team

and
Local Nephrology Team (if onsite)

 

 

 

 

 

 

These are Holding Measures Prior to Specialist Help from Critical Care

or Nephrology Services

 
 

All AKI All AKI
with with
Blood or protein on dipstick Obstruction on USS
Possible autoimmune disease/ (NB partially obstructed patients may have
glomerulonephritis, myeloma normal or high urine volumes).
Possible HUS/TTP, hypertension
Poisoning.
Renal transplant and CKD stage 4/5

Progression to AKI 3 Or AKI 3 at Recognition or AKI Complications
and Imminent Recovery Unlikely

Institute AKI Care Bundle While Transfer Pending

 
 

 

 

 

The following is a guideline for whether patients are safe to transfer from
a ward to a kidney unit in another hospital.
All AKI patients for transfer should be assessed by a senior (ST4+) doctor.

 

 

Hyperkalaemia
No ECG changes.
K < 6.0mmol/L.
If K lowered to <6.0 after presentation this must be potentially sustained (e.g bicarbonate therapy or
dialysis/CVVH) not transient therapy (insulin and dextrose).

Renal Acidosis
ayes
Venous bicarbonate >12mmol/L.
Lactate < 4mmoll/L.

Respiratory
Respiratory rate >11 and < 26/min.
Oxygen saturations >94% on not more than 35% oxygen.
If patient required acute CPAP must have been independent of this treatment for 24 hrs.

GT cab le icelay,
Heart rate > 50/min and < 120/min.
Blood pressure > 100mmdHg systolic.
MAP > 65MMbHg.
Lactate < 4mmoll/L.
(lower BP values may be accepted if it has been firmly established these are pre-morbid).

Neurological
Alert on AVPU score or GCS >12.

 

 
 

 

Requests for nephrology advice (not-transfer) on critical care
patients should be made to liaison nephrologist for the
hospital or, if unavailable, to local on-call renal team.

Referral for nephrology opinion is at the discretion of the
consultant intensivist and generally not necessary in patients
with AKI in the context of multi-organ failure.

Referral is recommended if

Possibility of AKI as an initiating event (with subsequent systemic
decompensation) - i.e AKI 3 early in illness.
Single organ failure.
AKI with possible vasculitis, lupus or autoimmune disease.
AKI in myeloma or malignancy or tumour lysis.
AKI with unexplained pulmonary infiltrates
or pulmonary haemorrhage.
HUS/TTP.
AKI in pregnancy.
AKI with urological abnormalities.
AKI with malignant hypertension.
AKI with poisoning.

 
 

 

Phone Local Renal Team

If the Patient is Accepted for Transfer, a Handover to Critical Care in

Receiving Hospital Should be Done and Critical Care Outreach Informed

Further discussion with receiving hospital intensivist not required if condition stable or improving

 

 

Below is a Guideline for What Would be Considered a Safe ITU to Kidney Unit
Transfer. These Transfers Should be Discussed at a Senior Level.

Metabolic
K < 6.0, ionised Ca > 1mmoll/L.
pH normal.
Bicarbonate > 16mmol/L.
Lactate normal.

Respiratory
Respiratory rate >11/min and < 26/min.
Saturations > 94% on not more than 35% oxygen.
If patient required acute CPAP must have been independent of this treatment for 24 hrs.
If ventilated <1 week should have been independent of respiratory support for 48hrs.
If longer term invasive ventilation should have been independent of all respiratory support
for 1 day for each week ventilated and for a period of not less than 48 hours.

Circulatory
Heart rate > 50/min and < 120/min.
BP > 100mmHg systolic.
MAP > 65MMHg.
If given inotropes given must have been inotrope independent > 24 hours.

Neurological
Alert AVPU (unless stable, chronic neurological impairment).

 
Referral from Primary Care

AKI 3 at recognition (creatinine 3 x baseline)
Local Renal Team

Direct Admission to Kidney Unit for Assessment

AKI 2 at recognition (creatinine between 2 and 3 x baseline)
Local Acute Medical Team

Follow AKI Care Bundle and Referral Guideline

AKI 1 at recognition (creatinine between 1.5 and 2 x normal)

Follow Primary Care AKI Bundle

 
Contrast Induced Nephropathy (CIN) Prophylaxis

 

 

 

 

 

 

 

 

 

Monitor Function To 72 Hours in High Risk Cases
If oliguria or rising creatinine early referral to local renal team.

NB there is no-proven role for N-Acetyl cysteine or post-contrast dialysis/CVVH.
Cessation of metformin should be considered if serum Cr above reference range or eGFR<60.
Cessation of ACE inhibitors should be considered if acutely ill.

 
Perioperative AKI

Preoperative AKI Risk Assessment

(anaesthetic and surgical teams) in pre-assessment clinic or ward

 

 

Postoperative AKI Risk Assessment

 

Monitor
Observations (blood pressure, heart rate, urine volumes, regular blood tests)

 

 

If postoperative AKI develops
Institute AKI Care Bundle and Referral Pathway

   

 
 

 

Baseline Requirements Give According
50-100mmol sodium, 40-80mol potassium to Clinical Scenario
and 1.5-2.5L water per 24 hours
Oral, enteral or parenteral route

Adjust estimated requirements
according to changes in sensible
or insensible losses

Sensible Insensible

Losses Losses

(measurable) exyelieena ce) a

Surgical drains Perspiration

Vomiting Metabolism

Diarrhoea Increase in pyrexia
Urine or tachypnoea
(variable amounts of (Mainly water)
electrolytes)

Regular assessment of volume
and hydration status
Daily weights

 

Fluid charts
Measured electrolytes

Available parenteral solutions
(if required)
Hartmans solution/Ringer’s lactate
Normal Saline
5% dextrose
0.4%/0.18% dextrose/saline
Potassium usually added additionally

 
Obstetric AKI Pathway

 

 

THIS IS POTENTIALLY A MEDICAL EMERGENCY

 

 

Monitoring in AKI

 

Investigations in AKI

 

 

 

 

Supportive AKI care

 
 

 
 
 

 

 

Causes Think STOP AKI
Prerenal Sepsis/hypovolaemia (PPH)
Renal Toxicity NSAIDS, PET, HELLP, HUS,TTP

Postrenal Obstruction or ureteric damage during delivery
AKI Teaching Materials

“STOP” - causes of AKI
Sepsis and hypoperfusion (hypovolaemia, heart failure, hepatorenal)
Toxicity (drugs, contrast)
Obstruction
Parenchymal kidney disease (myeloma, rhabdomyolysis, RPGN, HUS, TIN)

YN Upper Limit of Normal

BUN or Scr

Rising creatinine

oo 5 % Function (GFR)
= rising mortality

KDIGO Staging System for Acute Kidney Injury

Stage} Serum creatinine Urine output
rise => 26 umol/L within 48hrs <0.5 mL/kg/hr
or rise =>1.5- to 1.9 X baseline SCr | for > 6 consecutive hrs
rise => 2 to 2.9 X baseline SCr <0.5 mL/kg/hr
for > 12 hrs

rise >3 X baseline SCR <0.3 mL/kg/hr

or rise 354 umol/L for > 24 hrs

or commenced on renal or anuria for 12 hrs
replacement therapy (RRT)

irrespective of stage

 
Kidney Unit Contact

North Central London Network Hospitals

 

North East London Network Hospitals

 

North West London Network Hospitals

South East London Network Hospitals

South West London Network Hospitals

 

 
References:

National Institute for Clinical Excellence (NICE) Clinical Guideline 169:
Acute kidney injury: prevention, detection and management (2013).

National Institute for Clinical Excellence (NICE) Quality Standard 76: Acute
Kidney Injury (2014).

National Institute for Clinical Excellence (NICE) Clinical Guideline 174:
Intravenous fluid therapy in over 16s in hospital (2104).

Joint UK Renal Association, Cardiovascular Intervention Society and Royal
College of Radiologists Clinical Guideline on Prevention of Contrast
Induced Acute Kidney Injury (CI-AKI) in Adult Patients (2014).

London Health Programmes Standards on Inter-hospital Transfers (2104)>

Adding Insult to Injury. A review of the care of patients who died in
hospital with a primary diagnosis of acute kidney injury (acute renal
failure). National Confidential Enquiry into Patient Outcome and Death
(NCEPOD). 2009.

UK Renal Association Clinical Practice Guideline on Acute Kidney Injury.

2011.

Kidney Disease Improving Global Outcomes (KDIGO) Clinical Practice
Guideline for Acute Kidney Injury. 2011.

National Institute For Clinical Excellence (NICE) Clinical Guideline 50:
Recognition and Response to Acute Illness in Adults in Hospital.

. Imaging for Acute Kidney Injury (acute renal failure): Good Practice
Recommendations from the National Imaging Board. 2010.

. British Consensus Guidelines on Intravenous Fluid Therapy for Adult
Surgical Patients.BAPEN Medical, the Association for Clinical Biochemistry,
the Association of Surgeons of Great Britain and Ireland, the Society of
Academic and Research Surgery, the UK Renal Association and the
Intensive Care Society. 2008 - update 2011.

. Pre-operative Assessment and Patient Preparation: The Role of The
Anesthetist. The Association of Anesthetists of Great Britain and Ireland.
2010.

. Guidelines for the Transfer of the Critically Ill Adult. UK Intensive Care
Society. 3rd Edition 2011.

 
Acknowledgements

We would like to thank Chris Kirwan for preparing the section on
perioperative acute kidney injury, Danny Gale for preparing the section on
acute kidney injury in thrombotic microangiopathy and Anita Bannerjee for
preparing the section on AKI in preganancy.

We would like to thank Nick Macartney and Jeremy Dawson for their help
with the bundle checklist.

We would like to thank Health Education North Central and East London for
Sponsoring the development of the manual.

Chris Laing oversaw preparation of the London AKI Manual on behalf of
London AKI Network

Permissions

These materials are copyrighted. We are very happy for the contents of this
manual to be used in not-for-profit, non-London AKI Network materials.
Please contact us for permissions and please do credit us.

 
