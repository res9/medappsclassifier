[b]Programm:
[/b]
Impfen 2014



[b]Hinweis:[/b]

Die Medizin ist, wie jede Wissenschaft, ständigen Entwicklungen unterworfen. Alle Informationen dieses Werkes wurden mit großer Sorgfalt nachgeprüft. Der Inhalt der Impfen 2014 Application wurde mit Sorgfalt unter Modifikation des Impfplanes Österreich 2014, Evidenz-basierte Empfehlungen des Nationalen Impfgremiums des Bundesministerium für Gesundheit Österreich erstellt.[i] [/i]Impfen App bietet allgemeine und nicht verbindliche Informationen. Jeder Benutzer ist angehalten, durch Überprüfung der Beipackzettel der verwendeten Präparate sowie die Empfehlungen des Nationalen Impfgremiums des Bundesministerium für Gesundheit Österreich und gegebenenfalls nach Konsultation eines Spezialisten, festzustellen, ob die vermittelte Empfehlung für Dosierungen, Grundimmunisierung, Auffrischung, oder die Beachtung von Kontraindikationen und Dosisanpassungen gegenüber der Angabe in Impfen App, abweicht. Trotz sorgfältiger Kontrolle übernehmen wir aber keine Haftung für die Inhalte.  Geschützte Warennamen (Warenzeichen) werden nicht immer besonders kenntlich gemacht. Aus diesem Fehlen kann also nicht geschlossen werden, dass es sich um einen freien Warennamen handelt. Das Werk ist, einschließlich seiner Teile, urheberrechtlich geschützt. Jede Verwertung außerhalb der engen Grenzen des Urheberrechts ist ohne Zustimmung des Autors unzulässig und strafbar. Da der Inhalt des Programms  der allgemeinen Information dient, ist die Geltendmachung des Ersatzes von Schäden oder Unannehmlichkeiten, die durch den Gebrauch oder Missbrauch dieser Informationen, direkt oder indirekt, mittelbar oder unmittelbar, entstehen, ausgeschlossen. Der Autor erhebt weder einen Anspruch auf Vollständigkeit noch kann die Aktualität, Richtigkeit und Ausgewogenheit der dargebotenen Information garantiert werden. Der Text darf nicht als Grundlage zur eigenständigen Diagnose und Beginn, Änderung oder Beendigung einer Behandlung von Krankheiten verwendet werden. Jede Dosierung oder Applikation erfolgt auf eigenes Risiko. Alle Dosierungen betreffen Patienten mit normaler Nieren und Leberfunktion. 






