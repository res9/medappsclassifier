DL curved

”
by m
W-diolosa.co

 
NS
Vo
 
Mn SA

SnUSTERS
PNewronnuLs |

 
MUSTER |

 
 
 
 
MUSTER 4

 
 
 
 
 
J
 
ac |
|
(‘2
 
 
Diolosa Linie – für TCM Therapeuten 

Die Diolosa Linie wurde für TCM Therapeuten entwickelt und besteht aus mehr als 200 Präparaten.  Alle Rezepturen wurden von Claude Diolosa konzipiert und basieren auf seiner umfangreichen klinischen Erfahrung. Die Diolosa Linie ist für zertifierte TCM Therapeuten, Ärzte und Heilpraktiker entwickelt worden und kann nicht ohne eine Traditionelle Differentialdiagnostik  verordnet werden. 

Features auf einen Blick

-	mehr als 200 häufig vorkommende Syndrome mit Differentialdiagnostik und Rezepturen
-	die Rezepturen wurden nach Fachbereichen und Symptomen geordnet 
-	um Fehler zu vermeiden, wurden jedem einzelnen Präparat Pulsmerkmale, Zungenmerkmale, Symptome, Energetische Bilder, Kontraindikationen, Ernährungsempfehlungen und Kombinationsmöglichkeiten beigefügt
-	eine Schulungsunterlage als free download steht ebenfalls bei www.diolosa.com zur Verfügung  
-	eine Bestellung unter den angegebenen Konditionen kann sofort an die Apotheke Boznerplatz weitergeleitet werden
-	Wichtig: Dieses APPS ersetzt keine Diagnostik und keinen Besuch bei einem  vollständig ausgebildeten TCM Arzt oder Heilpraktiker 

Sämtliche Presslinge dieser Verordner–Linie (Diolosa Linie) haben ein Gewicht von 
350 mg und eine Packungsgröße von 140 Stück.  Einige Präparate, bei denen eine längerfristige Einnahme empfohlen wird, sind auch in einer Großpackung von 700  Presslingen erhältlich. Die Dosierungsangaben in den Schulungsunterlagen und auf den Verpackungen sind nur allgemein und dürfen immer von dem behandelnden Therapeuten verändert werden. Die Dosierung 3 x 1 bis 3 Presslinge am Tag ist eine Standard Empfehlung und darf je nach Bedarf erhöht oder reduziert werden. 

In den Schulungsunterlagen (www.diolosa.com und www.diolosa.info) und APPS wird die Differentialdiagnostik jedes einzelnen Präparates genau definiert. Das Energetische Bild ist die Essenz jeden Präparates und sollte immer sorgfältig studiert werden. Zusätzlich zu Puls- und Zungenmerkmalen, sollten diätetische Empfehlungen immer einen wesentlichen Platz in der Therapie einnehmen. Das Vermeiden von Nebenwirkungen liegt an der ausführlichen Beschreibung und dem Energetischen Bild jedes einzelnen Produktes. 

Alle Präparate können in Form von Dekokten und Presslingen bestellt werden.  Die Apotheke hat präzise Anweisungen über die Zubereitung (Pao Zhi) und das Dekoktieren aller Rezepturen und aufgrund der hohen Dosierung jeder einzelnen Rezeptur (manchmal 150g bis 250g) und dem neuen Dekoktier Verfahren (sehr konzentriert) werden aus einer Tagesdosis 1 Liter Dekokt für drei Tage Behandlung zubereitet. Die Posologie (Tagesdosis) liegt bei 3 x 100 ml am Tag. 

Apotheke Boznerplatz
Bozner Platz 7, A-6020 Innsbruck
Tel: 0043-(0)512-585817
www.apo-boznerplatz.at           
tcm@apo-boznerplatz.at

Application Design by:
Christian El Salloum (salloum@gmx.at)

Konditionen:
Die Abwicklung der Bestellung erfolgt über die Apotheke Boznerplatz in Innsbruck. Sie wird Ihnen auch die Rechnung für die Bestellung schicken.

Versandkosten: (Stand 09/2015):
Österreich:      € 4,55 (versandkostenfrei ab einem Einkaufwert von € 70.-)
Deutschland:  € 9,80 (€ 6,80 ab einem Einkaufswert von € 70.-)

Rabatt für Therapeuten:
Bestellmenge unter 5 Stück (sortenrein): AVP -10%
Bestellmenge ab 5 Stück (sortenrein): AVP -23%

Hinweis: Rabatte werden nur gewährt, wenn die Liefer- und Rechnungsadresse der des Therapeuten entspricht. Die optionalen Eingabefelder für eine gesonderte Liefer- und Rechnungsadresse müssen in diesem Fall leer bleiben.

Hinweis: Bestellungen aus der Schweiz unter 20 Produkten werden direkt an unseren Vertriebspartner in der Schweiz weitergeleitet. Preise finden Sie im Onlineshop unter www.diolosa-linie.de


Alle österreichischen Kunden erhalten automatisch eine Rechnung inklusive Mwst. Kunden außerhalb von Österreich mit einer gültigen UID - Nummer, die bei der Bestellung angegeben werden muss, erhalten die Rechnung excl. Mwst.

