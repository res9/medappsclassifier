Human Skeleton
Reference Guide

}
ot
om

 

Ww
luminous
apps
 
Pa)
ft
Maar
Tale:
oa
Sy
You
all
  
 
  
 
 
  
   
   
  
  
 
  
 

fibula iba

anterior tibiofibula ankle joint

ligament

talus
deltoid ligament

anterior talofibular

ligament navicular bone

cuneiform bones:
intermediate
lateral

medial

calcaneous bone

cuboid bone

tarsometatarsal joint
metatarsal
bones

phalanges
interphalangeal
joint
proximal phalanx distal phalanx

middle phalanx
radius

ulna

    
     
  
 
 
  
 
  

carpals:
lunate

; scaphoid
triquetrum trapezium

pisiform To trapezoid

hamate (hook) es: capitate

metacarpals

phalanges:
proximal
middle
occipital sagittal suture

superior nuchal line 4 parietal
external occipital protuberance ) lambdoid suture
inferior nuchal line

  
  
 
 
 
 
 
  
  
  
  
  
  
  
 
  
 
  
 
 
 
 
 
 
 
  
  
  
     
  
  
   
  
 
 
  
   
  
  
  
 
 
 
 
  
 
  
   
  
  

zoni temporal
ocelpltal. condyle mastoid process
superior articular process Dp
atlas (C1) articular capsule

posterior atlantooccipital membrane
posterior atlantoaxial membrane
articular capsules
transverse processes
lateral costotransverse ligaments
supraspinous fossa
coracohumeral ligament
inferior transverse scapular ligament
articular capsule
infraspinous fossa
internal intercostal muscles
intertransverse ligaments
internal intercostal ligaments
external intercostal muscles
transverse abdominal muscle
ulnar collateral ligament
lateral epicondyle
articular capsule
radial collateral ligament
thoracolumbar fascia (anterior layer)
iliolumbar ligament

axis (C2)
ligamenta flava

ist rib ™

_Clavicle

acromion

spine of scapula
head of humerus
greater tubercle
anatomic neck
surgical neck
scapula

 

 

humerus

12th rib
olecranon fossa
olecranon

radial tuberosity
posterior superior iliac spine
posterior inferior iliac spine

     

j y.
|

sacrum {| % center of gravity
ulna ——_{|
radius posterior sacroiliac ligament
head of femur iliofemoral ligament
greater trochanter sacrospinal ligament
neck of femur

ischiofemoral ligament
pisiform . :
intertrochanteric crest

ischial spine 5
‘ sacrotuberal ligament

ischial tuberosity en
gluteal tuberosity

yw dorsal sacrococcygeal ligament
femur

gastrocnemius muscle

 

  
  
   
  

medial femoral
condyle
lateral femoral . aon
condyle oblique popliteal ligament
intercondylar 5 Vy arcuate popliteal ligament
fossa s popliteus muscle
1 \ i semimembranosus muscle
tibia — 1 soleal line
, yi
fibula interosseous
| membrane
;
}
medial malleolus | | posterior tibiofibular ligament Key
talus \ t deltoid ligament Vertebrae
lateral malleolus posterior talofibular ligament € cervical
calcaneofibular ligament T thoracic
Saltaleus calcaneal tendon L lumbar

 

 

 
coronal suture
parietal

sphenoid

temporal
zygomatic
infraorbital foramen
maxilla

anterior longitudinal |.
anterior sternoclavicular |.
superior transverse scapular I.
coracoclavicular |.
acromioclavicular |.
coracoacromial |.
subdeltoid bursa
subscapularis m.
articular capsule
biceps brachii m. (long head)
internal intercostal mm.
external intercostal mm.
interchondral Il.

 

external intercostal membranes
thoracolumbar fascia
transverse abdominal m.
articular capsule
ulnar collateral |.
radial collateral |.
annular |.
iliolumbar |.
anterior sacroiliac |.
Inguinal |.
interosseous membrane

center of gravity
sacrospinal |.
sacrotuberal |.
iliofemoral |.

scaphoid

lunate

triquetrum AX

hamate \

capitate \

trapezoid

trapezium

 

obturator membrane

articularis genus m.

quadriceps femoris t.
tibial collateral |.

medial patellar retinaculum
fibular collateral |.
patellar |.

interosseous membrane

anterior tibiofibular |.
talus

medial cuneiform

 

  
 
  
  

frontal

outline of frontal sinus
. nasal
— . : : *, 7
a >= superior and inferior orbital fissures

J outline of maxillary sinus
mandible
CT st rib
manubrium
clavicle
acromion
coracoid process
greater tubercle
lesser tubercle
scapula
humerus
sternum
xiphoid process

costal cartilages

12th rib

intervertebral discs

anterior longitudinal |.

medial epicondyle
lateral epicondyle
trochlea
capitulum
radial tuberosity
outline of female pelvis
anterior superior iliac spine
anterior inferior iliac spine
radius
ulna

7 head of femur
transverse axis

greater trochanter

neck of femur
“SF carpals

— metacarpals

  

ischium
arcuate
pubic |.

 
    

phalanges

 

 

lesser trochanter

femur
medial epicondyle

patella
lateral epicondyle

transverse axis
lateral femoral and tibial condyles
intercondylar eminence
medial femoral condyle
head of fibula
tibial tuberosity
medial tibial condyle

tibia

fibula . ligament
. ligaments
. muscle
. muscles

. tendon
. tendons

medial malleolus

Vertebrae
C cervical
S sacral
L_ lumbar

transverse axis
lateral malleolus

 
frontal bone

  
 
 
 
 
  
  
  
 
 
 
 
  
  
 
 
 
  
 
  
 

supraorbital notch

coronal suture
supraorbital margin
nasion

parietal bone lesser wing of

sphenoid bone

temporal bone

superior orbital fissure
sphenoid bone P

(greater wing):

temporal surface .
optic canal

orbital surface
fossa for lacrimal sac

zygomatic arch ogg oy a
inferior orbital fissure

zygomatic bone
temporozygomatic suture

nasal bone

infraorbital foramen inferior nasal concha

perpendicular plate anterior nasal spine

of the ethmoid bone

mandible

maxilla
mental foramen

intermaxillary suture

sagittal suture

   

parietal foramen

lambdoid suture

interparietal bone parietal bone

(Inca bone)
sutural bones

squamous suture | transverse

occipital suture

parietomastoid suture temporal bone

occipitomastoid suture occipital bone

mastoid foramen superior nuchal line

mastoid notch mastoid process

inferior nuchal line

styloid process external occipital protuberance
external occipital crest
