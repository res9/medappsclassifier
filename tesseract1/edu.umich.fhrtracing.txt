 

Histology of Blood and Bone Marrow —
A Second! 6ok™ Series

Jump To Slide

ULES A UMUC ce)
WT Cer THT}

OPP a RS cmon

EAE ed ry

 

 

 
Review Topics

  
  
 
  
 
 

@ Blood and Bone Marrow
Bone and Bone Formation
Cardiovascular System

eo Cartilage

@ Cell Biology
Connective Tissue

Central Nervous System

(0) (o)
rv All

 
eo ———————

Central Nervous System

© ®
All rv
»

 
   

@ help &
Review Topics About

» Register
Blood and Bone Marrow

@ orientation

Bone and Bone Formation
Lock

Cardiovascular System a
Cartilage

Cell Biology

Connective Tissue

Central Nervous System

 
 

 

Histology of Blood and Bone Marrow —
A SecondL6ok™ Series

Michael Hortsch, P’
Department nd Develo! Biology
“ iD

 
     
   

  

y of Mic
LOW

Pe ET
Yai)

COPPA Cau Renee aene Uy

ae ed ry

 
 

 
Briefly summarize the most important
functions of this cell fragment?

Pere) @o t
 

 

Histology of Blood and Bone Marrow —
A SecondL6ok™ Series

Department > Cell ‘ -_. Biology

Taree Michigan
hortsch( Talen=ye [0]

    
   
 

n

PUPS eRe eed ene

ae ed ry

 
 
acl Heart Rate ibclely °

 
Introduction, Baseline, and Variability -
A Fetal Heart Rate SecondLG6Ok™ Series

Maxwell Spadafore and Maya Hammoud, M.D.

Departments of Obstetrics and Gynecology and of Learning
Health Sciences, University of Michigan Medical School

Editor: Michael Hortsch, Ph.D., Departments of Cell and Dev. Biology
and of Learning Health Sciences, University of Michigan Medical School

University of Michigan
©2019 Spadafore & Hammoud
 

Sete ae ts ae

and_____
 
When internal tocometry is used, the y-axis of the lower
graph represents in units of

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

14 mua r SFY a ERY ema
: a : a : : 5 a
| - | bese t a ;
: siti : eel) ‘ : ca
| i | ane | | =
est.) eet a 0
+ + i | hn Tt Vaan : Land
mee : : ; rie rie
- I) : ee el)
| | j { nee i p
H 60 H ar 60
| j | Foret p
; 30. 7 0) 30.
a ced Es er EEE PEELE PPLE PEEL Pign PEP]
Ca EX EX
if { f i [ : i
Pee AEH aL ceae eee 7 CceEnae EES
Ca + ty 1 : : ri : :
a f Bea rae 1 ‘ t pe
atin hy iti ti 1 SE TP I i en a i

 

 

 

 
 

 
What unit is used to assess uterine performance over time?

 
 

Da Ae or once a URC uCSei og}
a
Dea Flatts

The information in this review resource was
developed for educational purposes only. It is not
meant to be used for or considered to be
diagnostic or therapeutic advice.
ie lll)

peak.
 

 
Diagram of a normal fetal heart tracing

The x-axis represents what?

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

yin Gee ea RIEVIB GEL aaD SEESIFeVinGEELNS al
ree j 210 : sc
+t ieee : | roomy ;
eee } te t ca
if 1
Eis | | ESeel 2
i ft as ane Seana aoeeee imate.
mee : ; ru | ; 220
ely ct } Y
| f { ea | p
rs) } re t ch)
t 1 i jet | i 1
; cd ; cid : acl
ces 5 a 5 SSS ee ee
ae a eo et , ae
ri od t t
mer me oS f
al 40 ! a ot
| } , | i 2 |
anh iti ti in ew ny i i pene I

 

 

 

 

 

 
 

 
Diagram of a
normal fetal
heart tracing

The light blue line in the top graph
represents... ?

 

 

 

 

14 at BHR 2 20pm T 11:40 1s peRea open T Ry i awa a
 TESEca ae =a ees
nas Saaere Ea Sane
80-4 seat =eaee oe
tl | | | )

 

 

t ia at soe aa Sea ta coeeeeee ae i eral
yl i ——-120. T t A
i i i i

 

 

 

 

 

a oa) i i
90— } al) eT
a iam | i
a } et } 60
TH | eee i p
30. 7 0) 30.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
3 cm/min

 
 

Indicate 10 bpm in the insert.

 
 

 
 
How many contractions do you count in the 10

minute window indicated?

 

Seema PT UE

 

Peete UL

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

14 Fj AMD cre ees ha
210. ra t a
= soe t a a
eee te t ca
| | Sia
EY a i i caer ete
; = aaa etal Ve ei
meee — ru ; ; ; bouuaed
a tt) i) - i nee
| i p | ites
: a a | t | 60
i jem | aeee
30 30— - en
—-1
‘ n y —
‘ eo i i et ae
1 ale ry i
. i . i
i Scare ei | et al
T Aaa | Saeran Tae
ay i i t i

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

meee le leary
RISER UMNO Rae] ig
>5 per 10 minutes
That abnormality would be called ...?
Determine the uterine contraction rate in the
strip below?

 
 
IN OMT SE-eaTOUC-LLWARerol| -re BAI
tachysystole.

Determine the contraction rate in the strip
let=)(o\ ita

 
 
re muah ot)
sen UD eC Meena kl Rg

 
110
eae oR)
roi a Ped

 
 
 
 
The baseline in the strip below is?

TROIS EEN STRONY ERR A en NaNO ea

 
 
 

ctu
Foy R Va MBB a tlt R ATA
 
 
 
Identify the baseline in the strip below.

 

 

 

 

 

 

 

 

 

as (GHA eee =a HY ie Gee ee EEE
-210. 1 rah ; , ; , 210 ,
= | of |
SEL) t Ta | t stl
| | Ut
-150. Saolty - eon lees
| ; ce
1 aica aa BL: (Oy ac een CPE
ni et A a i aA
iy U it if iu
0 90 | t | eo
if if uJ
~60. C0 | { 0) |
p i | p
els 30. - - ie) i

 

 

 

 

ae i ae

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

2 eaeeeariyat aan nal a a
60: t ; 60: ,
Ne ‘ IN Sean AS Sa eas
any aa a Lape esee ey mas
t

i i :

EM entre i ‘UA-O-mmHa.

 
 
 
 

eon oy
seosasass
 
 
Estimate the baseline in the strip below.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Top preese yee sobee CEOS MS REDE CE

t t eam oto i Er

| i aT 10 ‘180

| f Tr t | er ie i i
ee : en ee ee one een] (Se a ee oe AR

1 eo 120 7 |

+ - Jie a Xt) ES + Hf

il i UJ 1

| | a Ca | |

t i | y j

u EI Eid

a iia aD Oo ca Sa a 7 7 a

} im oie su

: 60— A ) 0

see as eieee aesogaREg On Pople ons

f | am 4 } x
ee eee ee Teen Men Cey lore

 
SR Uo aa hel ols

patie
 

Mie oRirel ues el}
Mle Col Tat Al aU Mie MISO? (= MUL ROLL ok
BS TESS BC oo Tea OMe UL eo
Estimate the baseline in the strip below.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

ea (FIO Ie aurea SET Ia amare ERR
ray 7 rato
i io i i i { aid (mm
oct) eee ee
ei eae eae ar Te
am Va Vi ay) Naaee eanesy raver
: | tt i
B SSSs Coes cena season ee onee 90
AS Esesascecess a once i gneee ee neaer anosoceaaencead
) | | p i | | jee
ag i
2 Method: TOCC le 30
Le ee ee cu a J eeuee rr ae es A
1 cra 1 t a |
i re | i | Cy |
— eet : wl ;
20 - f xd {
LOM e er it Ty On !

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
The bright green line in the lower graph represents

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

what?
14 aera SIFTS aera cain maa a
| 210 aly Pato
= soe a i
180. Ee : : me t
i
YY i eet 0
ea ; acho
1 ru 1 efece
| |
i) ct) '
p i p
oe | ia
EI) en
soo ee rom oY a
a i 80 aa
me 60 if
ry f . ny al
20 ace aeee Peet
aye ee en ee

 

 

 

 

 

 
 
 
 
 
 
—
Accelerations and Decelerations -
A Fetal Heart Rate SecondL6Ok™ Series

Maxwell Spadafore and Maya Hammoud, M.D.

Departments of Obstetrics and Gynecology and of Learning
Health Sciences, University of Michigan Medical School

Editor: Michael Hortsch, Ph.D., Departments of Cell and Dev. Biology
and of Learning Health Sciences, University of Michigan Medical School

University of Michigan
© 2019 Spadafore & Hammoud
CNR ere Ree Ceo)
TTR BrcCene Creel CL nf)

 
——e

 
 
 
highlighted

3 cm/min

 
Dea Flatts

The information in this review resource was
developed for educational purposes only. It is not
meant to be used for or considered to be
diagnostic or therapeutic advice.
3 cm/min

 
 
 
 
 
 
00:10

 
 
15 bpm
acceleration lasts at least___, but less than _.
 
Fetal heart rate (FHR) tracings contain different
baselines, variabilities, accelerations, decelerations, and
more, but what does that mean clinically? What is
reassuring? What requires further observation or action?

To aid decision making, there are_____—sFHIR tracing
Categories, organized from most to least ?
early decelerations (decels) late decelerations
oer nal
 
ace ot and must have
rere EY tracing

 
 
 
 
3 cm/min

 
 
acidosis or
Lihat ona
 
There are specific rules defining Category II fetal
heart rate tracings (FRTs), but the following
definition is much easier:
A Category II tracing is
fe Tole Aare lel eA Loe
ae SALA ee Bed ke)

 
 
 
laa

enon
 
FHR Tracing Problem Set -
A Fetal Heart Rate SecondLOOk™ Series

Maxwell Spadafore and Maya Hammoud, M.D.

Departments of Obstetrics and Gynecology and of Learning
Health Sciences, University of Michigan Medical School

Editor: Michael Hortsch, Ph.D., Departments of Cell and Dev. Biology
and of Learning Health Sciences, University of Michigan Medical School

University of Michigan

©2019 Spadafore & Hammoud
Analyze the below fetal heart rate (FHR) tracing:
Determine its baseline:

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

SEEE LHe aeecoctael FO Seam RLMaiacal SE EIEUEE GREE.
ee a - 210. ~
=m | ! |
ea SEL Cees |
{ } } i
EY sos
Byeeesst sa EEGs re EEEHEEEPPZEEEEC ECE
Z aie f
Sz eseeneaeaseel iv Enaees
=o } oy ,
i i
mele - 30. .
— 109 7 ce 100
ooo mt ia l om imal aa ;
: as 80} u Ey
—— aCoeee 60 f cee
! os rl t ae ym | _ mA
| Pr oF oe 1
am i ar fl | A mA

 

 

 

eee e ‘U2.0-mmHa_L Te ents [Lt TW een Pe

 
its FHR
: ee nm

 
 

nen
Analyze the below fetal heart rate (FHR) tracing:
Determine its baseline:

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

eee) a : . a y
eae rma oleae a 72h) ae ae ar
1 ray aK ae) a
aie j ‘ia i { aid j
180 i | 180—
| |
i Ast} -150. 1 ‘g ont as i cht
t f of ; A U ao 2 ie
ees aw * ae wR
) Mi cae on | ee i : ia hue py a
| i 3
Y ed i 90
f D 1
| 60. | a0 | 0
f j 1
i cI MY YY
a ae ToL = rae == i a n
J i :
60 f ne nN ca
40 i) : Eo
aa | MS i f °
: Ae : i | 20
net rene i ie en

 

 

 
 
 
 
Dea Flatts

The information in this review resource was
developed for educational purposes only. It is not
meant to be used for or considered to be
diagnostic or therapeutic advice.
 

eel
STE Rl here WU Rae
 
Analyze the below fetal heart rate (FHR) tracing:
Determine its baseline:

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

FHR 240 bpm " os) ms Ey 7s. FHR 240 bpm- 08:36 7 FHR 240 bpm: i"
f | a i i Bs} i |
tT aio i 210. } i Lato. }
: i f met 4 : }
ota cE | i 180
i \ i =m | 1
ha } 1150 | ‘ 150 |
Seeapees aint: Saas nee aaa nee ieee
ooe= Secaee Eaeecaeree Aaas i NEE 7 =a
i UJ i i 1 i | iu
60 Co | f et
i p | iam | | nee |
i Et L clot
100574 = : 10
mai oR a ee ni | a eee ee Gee
t ae 1 ar t }
; ; ee ; eal ae
t r i T } mes i .
| edeena 20a a | |
alig- l ____UA.0-mmHg- i -UA-0-mmHg- 1

 

 

 

 

 
Analyze the below fetal heart rate (FHR) tracing:

Determine its baseline:

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

eet i BErEEE Buda PET IS uel eer
=u u f aa i es a)
re | } er | Ries crt AN
ea | a re ul | a : if bees
ar Caan we ene ae | tas cee acim es
ce ¥ f 120 1 i ey
a) i t la aia EN)
a) iol i i f i f
mr) t ! Co t a
ian i | D | i f
a 30 Sed gielea
eee
a Geel ae olan fa — oo sams . oa
| i ical Lett ia i a
| | [ | p A | } d
Fubar Hat? Slee aera
Seen a
Pal I i eh ea

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

OPM ee

 
Analyze the below fetal heart rate (FHR) tracing:
Determine its baseline:

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

a 7 FIL a eae aa SEP Ge a
eats | a Pit aio tt | i: f cast
rt | sd | i ana ipa
rt 1 Tea | Et)
it i i i if i i i
set | tee | | ee
NV Siig Se aan aa ace } Perera a
vm Mer ot : ee oer paeod
i) i T i if i 1 i
oe aoe caee aeer le
“60. ase neeesaseaeeaee OsSEeeete
p | p i | i | ie
30! 30 i Ei t
Peel aa aT alll a oo
1 | oo 1 t ; 80 oo
1 1 i) 1 0 t I
| Naren re N i iN aan oie mat
i 5 i i i 5 I. i
a BRESes iu BsScetGresin ta tesere: ats ho nana WC aaley Serena
I il bee Menrniistee it I my rn

 

 

 

 

 

 

 
 
 
 
 

; PR tile
 
 
eu
for esim came eeclccna Pee

re
 
 
Analyze the below fetal heart rate (FHR) tracing:
Determine its baseline:

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

aml anal r SIFY ile eee aaa ERY ema
eeu ee aero
nsT:T) { | 180-1 i tre
if I | 1 i i if i i i
PEL aun oe ' ae 0
‘ i aaa etal Ve
; ru 7 a ru ; - sr)
i i i T
, tt) , ora - ct)
i i | 1} | i
7 60 H ae 60
f f p
: 30 : 30— cI)
1 oT 100 7 aa To Ose
ae a eo 1 CT ae
60-4 or) t t
pars | me oT} t
oy ; ard ZU | A cd t ial
xa L ce : 1 |
anh iti ti in ew ny I i en a I

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
Analyze the below fetal heart rate (FHR) tracing:
Determine its baseline:

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

IR240 bpm: ps] FHR 240 bpm i 7 ae mee 21S Sy
21:00 Hi me alee) | FFT]
a0 L Zoe sa ; : ai ai
=e i | alt |
it STI} t ett |
i i I i IT if
150— oy 5 7 ees) eaeanee 7 ;
ea encascancues Samra ' a
Po sabeesod sea) : Eo saan
: | ae pESeeaeees |
=a | ae AEE REE ARE Ser S| cease ae anaes
UJ i T if i i Ls if il it
eel cael aoa see
‘ ‘
il i i
30 Method: TOCC si 7 30
ea
20 ——100 0.
i a a — al eT
co a Oee om l t t psa i }
r i 1 : 60: : :
mea j 40
oe ae Se eeee See eaneenceeaeaes
cmap Manat te

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 
 
 
 

eeK Ae ala peel. NV
 

orca
Category ltracingsare
 
Analyze the below fetal heart rate (FHR) tracing:
Determine its baseline:

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Pea ez a x
i enema i co eee meee EEDE
+ au 210. a a
t =f ett i =2
i 1 tT) i
i \ i
+ rey cE} a |
ian Goin mae ee poet a os z
. sas i rer a aa ans
i i j | j | |
7 oy oY | |
| j j cal i
: 0) ; 60. - — :
| | EE |
Method: TOCC 30 a
a TI = ae a eee oe elm
ed EY ; oY
re 60— eno 7 os rans Gy
f cd : : { od a iN 40
coe a aren nt a wt ain nee

 
 

 
 
 
 
letra
MVR Ul Maree Mer-1Colow AR UcCesTe 4

 
eee Mell a Roma)
Analyze the below fetal heart rate (FHR) tracing:
Determine its baseline:

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Pera } (OFFIC) Oa cas ve EERO taae maken wt 03:0
-210. ; ; 210 aK
+ ae t i i ca [pina
Shu ty t Ee)
a i i i |
a eae i eeereas Y Teeneainase Seay eae
y : it ‘ +
Be t ay ae he beeriag te Ca
j | -t ts oF i | | | fam
zy) t | i : 90
Seen SSeS Se Se eee SE Ee SE SEE DELS EE aE S EEE EEEESEEi
~60 oo i 60.
j p Et f
ag it it
2 Method: TOCC ; le ae
an a 2 rr ae es A
f | 1 i
rk t ee) :
6 eee oe Geena jo eeeaee
m7 : poet : :
+ 20 i meer) i i i
LOM e er ! ae ee it

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 
 
 

Set ee te
 
 
reac yal eer ert

 
 
 
Fetal Heart Rate Tracing -
A SecondL6ok™ Series

Maxwell Spadafore and Maya Hammoud, M.D.

Departments of Obstetrics and Gynecology and of Learning
Health Sciences, University of Michigan Medical School

Editor: Michael Hortsch, Ph.D., Departments of Cell and Dev. Biology
and of Learning Health Sciences, University of Michigan Medical School

University of Michigan
COS eee Meet}
Dea Flatts

The information in this review resource was
developed for educational purposes only. It is not
meant to be used for or considered to be
diagnostic or therapeutic advice.
