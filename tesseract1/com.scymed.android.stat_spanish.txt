 
 
 
eHa&P,”
ech
Ae
 
 
yoo.)

I
lal
| Pe
EKG-card

«Electrocardiography»
ScyMed
Be
 
 
 

 
 

 
 
 

 
P|
i
ICU-card
y) Tice U Rear!
H&P-card
(G) Celiiiateeance po
Stat-Spanish’

Spanish for
«Healthcare Professionals™»
 

Stat-Spanish
oO Wstat-Spanish

 
Ad SEPA Aad

«Spanish for Healthcare Personnel» =CAnGh
6th ED ScyMed.com

GREETINGS ANATOMICAL TERMS

‘Hello ==——_—sHHolla (oh-/ah) abdomen abdomen (ahb-doh-mehn)
Good morning Buenos dias ankle tobillo (Toh-bee-yoh)

(bweh-nohs dee-ahs) a pea ese
afternoon _ tardes (tahr-dehs) back espalda (ehs-pahl-dah)

evening noches (noh-chehs) belly barriga (bah-tree-gah)
‘Goodbye —=—— Adis (ah-dee-ohs) bladder vejiga (veh-hee-gah)
See you soon Hasta pronto (ahs-tah prohn-toh) blood sangre (sahn-greh)
How are you? Como esta usted? body Cuerpo (koo-ehr-poh)
(Koh-moh eh-stah oo-stehd?) bone hueso (weh-soh)

| bowels intestinos een-tehs-tee-nohs)
Very well muy bien (mwee-byehn) brain cerebro (seferefe bron)

‘Thankyou Gracias (grah-syahs) breast seno (seh-noh)

You're welcome De nada (deh nah-dah) calf pantorrilla (pahn-toh-rree-yah)

ae Si (see) chest pecho (peh-choh)
No (noh) ear oido (oh-ee-doh)

Maybe tal vez (tahl vehs) eyes, ce rans
Please por favor (pohr phah-vohr) eyelid parpado (SERCenECCE

Do you speak english? Habla inglés? face cara (kah-rah)

(hah-blah een-glehs) finger dedo (deh-doh)

LO ee ce ee ede a vesicula ere EER

(hah-bloh oon poh-kee-toh) an de mance (mah-nohs)

Do you understand? Entiende usted? head cabeza (kah-beh-sah)
(ehn-tyehn-deh oo-stehd?) heart corazon (koh-rah-sohn)

I don't understand No entiendo hip cadera (kah-deh-rah)
(noh ehn-tyehn-doh) kidney rifién (ree-nyohn)

Repeat please _Repita por favor knee rodilla (roh-dee-yah)

le ierna ee-her-nah
(pha-vohr deh reh-peh-teer) on higado peor ay

lungs pulmones (pool-moh-nehs)

mouth boca (boh-kah)
IDENTIFI CATI ON muscle musculo (moos-koo-/oh)
|What is your name?) Cémo se llama? neck cuello (kweh-yoh)
. nose nariz (nah-rees)
(koh-moh she yah-mah) Pancreas pancreas (pahn-kreh-ahs)
My name is... Me llamo... pelvis pelvis (pehl-vees)
(meh yah-moh...) penis pene (peh-neh)
First name Nombre (nohm-breh) prostate prostata (prohs-tah-tah)
Last name Apellido (ah-peh-yee-doh) rectum recto (rehk-toh)

Nice to meet you Encantado (ehn-kah-tah-doh) 0 im gosta rea”

Are you married? —_ esta casado (a)? sinus senos (seh-nohs)
(ehs-tah kah-sah-doh (dah)) spine espina (ehs-pee-nah)
... Single? soltero (a)? sohi-teh-roh (rah)) spleen bazo (bah-soh)
.. divorced? — divorciado (a)? dee-vohr-syah-doh (dah)) skin piel (pyehl)

i ? i! ? vee-ooh- skull craneo (krah-neh-oh)
awidow? | viudo aoe ey stomach estomago (ehs-toh-mah-goh)

(kwahl ehs soo dee-rehk-syohn) aah denies eae

Your phone number? Su numero de teléfono
p (soo noo-meh-roh deh the-leh-phoh-noh) tot garganta Ceoeeaeot
Your occupation? Su ocupaci6n?... urine orina (oh-reee-nah)
(soo oh-koo-pah-syohn) uterus Utero (00-teh-roh)
Do you have insurance? Tiene seguro? vagina vagina (vah-hee-nah)
(tyeh-neh she-goo-roh) waist cintura (seens-two-rah)

MISCELLANEOUS TIME & # RELATIVES
Who Quign” {(kee-ehn) cero (she-roh) Relative pariente (pah-ryehn-the)
at ue el !

uno (00-noh) Mother madre (mah-dreh)
Where Donde dohn-deh

dos (dhos) Father padre (pah-dreh)
tres (trehs) Sister hermana (ehr-mah-nah)
How much Cuanto men
alone ot een

cuatro (kwah-troh) Brother hermano (her-mah-noh)
cinco (seen-koh) Son hijo (ee-hoh)
seis (seh-ees) Daughter hija (ee-hah)
siete (syeh-the) Husband esposo  (ehs-poh-soh)
ocho (oh-choh) Wife esposa_ (ehs-poh-sah)
Why Porqué (pohr-keh) nueve (nweh-veh) Friend amigo(a) (ah-mee-goh)
10. diez (dyehs) Doctor doctor (dohc-tohr)
“minute minuto (mee-noo-foh) Nurse enfermera (ehn-fehr-meh-rah)
hour hora (oh-rah) Social trabajador social
Silence silencio _ (see-lehn-syoh) Day dia (dee-ah) Worker (trah-bah-hah-dohr-soh-syahl)
isolation aislamiento Night noche (noh-cheh) Interpreter traductor
Opendoor __ puerta abierta Week semana(seh-mah-nah) Dietitian dietista  (dee-eh-tees-tah)
Closed door puerta cerrada Month mes (mehs) Lawyer abogado (ah-boh-gah-doh)

OCONOOhWN=AO

Famil familia fah-mee-lee-ah

 
BASIC HISTORY SYMPTOMS & DISEASES

Cual es su problema? Asthma astma (ahs-mah)
Como se siente? Bleeding sangrado (sahn-grah-doh)
Cuanto tiempo? Breathing respiracion (rehs-pee-rah-syohn)
Tiene usted Bronchitis _ bronquitis (brohn-kee-tees)
alergias? (ah-lehr-hee-ahs) Bruise contusi6n (kohn-two-syohn)
Aqué? (ah keh) Cancer cancer (kahn-sehr)
F Cold frio (free-oh)

..any pain? Alguin dolor (ahi-goon doh-lohr) Cold gripa (gree-pah)
Do ae ema one i) Constipation constipacién —_(kohns-tee-pah-syohn)

_ Cough tos (tohs)
Toma usted alguna(s) medicinas? Cramps calambres (kah-lahm-brehs)
Do you smoke? Fuma usted?

Diabetes diabetes (dee-ah-beh-tehs)
Do you drink? Toma usted alcohol?

Diarrhea diarrea (dee-ah-rreh-ah)
Do you use drugs? Usa usted drogas?

Diet dieta (dee-eh-tah)
Previous surgeries? Cirugias previas? Dizzy mareo (mah-reh-o)
Tiene usted...

Edema edema (eh-deh-mah)
...high blood pressure? ...presi6n alta? Emphysema _ enfisema (ehn-fee-seh-mah)
. heart problems? __ ...problemas cardiacos?

Faint desmayo (dehs-mah-yoh)
...blood sugar? ...azucar en la sangre? Fasting ayuno (ah-joo-noh)
..lung problems? _...problemas pulmonares? Fatigue fatiga (fah-tee-gah)
«cancer? ..cancer?

Fever fiebre (fee-eh-breh)
Have you been in the hospital before? fitness condicion (kohn-dee-syohn)
Ha estado en el hospital antes?

Fracture fractura (frahk-too-rah)
BASIC PHYSICAL EXAM

Goiter bocio (boh-syoh)
Headache cefalea (seh-fah-leh-ah)
Please undress and put this gown...
Por favor desvistase y pongase esta bata...

How are you feeling?

Do you have...
...allergies?
To what?

Health salud (sah-lood)
Heartburn acidez (ah-see-dehs)
Heart failure insuficiencia cardiaca

~N

lam going to examine you...
Le voy a hacer su examen fisico...

 

What is the matter?
Show me

Relax

Don't move

Permit me

Sit down/sit up
Lie down
Stand up

Turn over

Move a little bit

Look straight ahead
Swallow

Open your mouth
Stick out your tongue

Breathe in and out
Breathe deeply
Hold your breath
Cough...again..
Lean forward
Does it hurt when

| press here?
Squeeze my fingers
Move your arms
Push out / Pull back

Se SES

Please help me...
--.out of bed
..to walk
. Sit in the chair

lam hungry

lam cold/hot

lam in pain

I need...water
...my dentures

...my glasses
...a telephone
| am better/worse

> Health 101°

Qué le ocurre?
Enséfieme
relajese

no se mueva
permitame
siéntese
recuéstese
parese

voltéese

Muévase un poquito
vea hacia el frente
trague

abra la boca

saque la lengua

respire adentro-afuera
respire hondo
aguante la respiraci6n
tosa...otra vez..

Inclinese hacia adelante

Le duele cuando le
oprimo aqui?
apriete mis dedos
mueva sus brazos
empuje / jale

por favor ayudeme

...a bajarme de la cama

..a caminar

..a sentarme en la silla

tengo hambre
tengo frio/calor
tengo dolor
necesito...agua

..-mi dentadura

...mis lentes
...el teléfono
Estoy mejor/peor

Health Calculators for Fitness & Wellness
Permalink: http://;www.ScyMed.com/health101.htm

©2008 A. Arturo Rodriguez, MD
Stat-Spanish card is a trademark of ScyMed.

6th ED. All rights reserved. Printed in USA
PO Box: 20233, Houston, TX 77025

High blood pressure presion alta (preh-syohn ahl-tah)
High Blood sugar aztcar en la sangre

Hot

Hungry
Injured
Itching
Jaundice
Medicine
Menstruation
Obesity obesidad
OD over dose sobredosis
Pallor palidez
Pregnant embarazada
Pulse pulso

Scar cicatriz
Seizure convulsi6n
Sinusitis sinusitis
Sneeze estornudar
Sore dolor
Sputum flema
Surgery cirugia
Stitches puntadas
Swelling inflamacion
Temperature temperatura
Thirst sed

Throat garganta
Tired cansado
Trauma trauma
Urine orina
Weakness __ debilidad
Wound herida

caliente
hambre
herido
comezon
ictericia
medicina

menstruacion

(kah-lee-ehn-teh)
(ahm-breh)
(eh-ree-doh)
(koh-meh-sohn)
(eek-teh-ree-see-ah)
(meh-dee-zee-nah)
(mehns-troo-ah-syohn)
(oh-beh-see-dahd)
(soh-breh-doh-seas)
(pah-lee-dehs)
(ehm-bah-rah-sah-dah)
(pool-soh)
(see-kah-trees)
(kohn-vool-syohn)
(see-noo-see-tees)
(ehs-tohr-noo-dahr)
(doh-lohr)
(fleh-mah)
(see-roo-hee-ah)
(poon-tah-dahs)
(een-flah-mah-syohn)
(tehm-peh-rah-two-rah)
(sehd)
(gahr-gahn-tah)
(kahn-sah-doh)
(trah-ooh-mah)
(oh-ree-nah)
(deh-bee-lee-dahd)
(eh-ree-dah)

OUTCOMES

You will need...
Medication
Surgery
To stay overnight
Lab work
An injection
An X ray
The flu shot
To call tomorrow
To return here
an appointment

Usted necesitara...

Medicinas

Cirugia

quedarse en la noche
analisis de laboratorio
una inyeccién

una radiografia

la vacuna del flu
hablar mafiana
regresar aqui

ISBN 978-968-5325-45-5

una cita
il |

9789685 325455

72) _ Stat SPANISH-card’)

 
 
ery
Ee
[ia
 
Be

a
ft
,

|
iscyMed® MediCalc® 8

Medical Calculator System’
Stat-Spanish™

HEE Eee ietso eels
cele lak
 
