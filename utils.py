from argparse import ArgumentParser
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import pickle
import json
import pprint

from common import clean_text, get_pipeline


def main(*, training_path, prediction_path, model_path):

    # input
    with open(training_path) as f:
        training_desc = json.load(f)

    with open(prediction_path) as f:
        predict_desc = json.load(f)

    # process
    X = [trainingdata]
    y = [Label]
    Z = [trainingdata]

    predict_namexml = [predictingdata]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=2)

    pipeline = get_pipeline()
    pipeline.fit(X_train, y_train)

    # output
    pickle.dump(pipeline, open(model_path, 'wb'))

    print("Accuracy is: ", accuracy_score(pipeline.predict(X_test), y_test))
    for index, p in enumerate(pipeline.predict(Z.values.astype('U'))):
        print(predict_namexml[index], p)


if _name_ == '_main_':

    parser = ArgumentParser('Description training')
    parser.add_argument('training_path', str)
    parser.add_argument('prediction_path', str)
    parser.add_argument('model_path', str)

    args = parser.parse_args()

    main(
        training_path=args.training_path,
        prediction_path=args.prediction_path,
        model_path=args.model_path
    )